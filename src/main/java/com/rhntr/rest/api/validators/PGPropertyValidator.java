package com.rhntr.rest.api.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.rhntr.application.dto.AddressDTO;
import com.rhntr.application.dto.PgDTO;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.StringUtil;

@Component
public class PGPropertyValidator	implements	Validator	{	

	@Override
	public boolean supports(Class<?> clazz) {
		return PgDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {

		/* Default validation for null or empty inputs */
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "enter pg name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "please select type of pg.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "foodType", "food service is not selected");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "advance", "deposite amount is not entered");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "negotiable", "selected amount is negotiable or not.");
		
		PgDTO pgProperty = (PgDTO) object;

 		if(pgProperty.getUser() == null || 
 				pgProperty.getUser().getId() == null)	{
			errors.reject("user not logged in");
		}

 		if(CollectionUtil.isNullOrEmpty(pgProperty.getSpaceProperties()))	{ 			
 			errors.reject("sharingOptions", "Available sharing is not selected");
 		}

 		
		AddressDTO address = pgProperty.getAddressSpace();
		if(address == null || StringUtil.isAnyNullOrEmpty(
				address.getPlaceId(), address.getFormattedAddress()))	{
			errors.reject("pgAddressHolder", "select the location of property.");
		}
	}

}