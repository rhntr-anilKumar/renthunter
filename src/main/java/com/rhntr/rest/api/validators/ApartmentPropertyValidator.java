package com.rhntr.rest.api.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.rhntr.application.dto.AddressDTO;
import com.rhntr.application.dto.ApartmentDTO;
import com.rhntr.utils.StringUtil;

@Component
public class ApartmentPropertyValidator	implements	Validator	{

	@Override
	public boolean supports(Class<?> clazz) {
		return ApartmentDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {

		/* Default validation for null or empty inputs */
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "enter home name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "constructedOn", "home contruction date required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tenant", "specify type tenants allowed");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bhkLength", "home space in BHK");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "deposit", "enter deposite amount");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rent", "enter rent amount");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "agreemantDuriation", "agreement duriation requied");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "buildUpArea", "build up area measument requied");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "negotiable", "negotiable of rent amount is requied");
		
		ApartmentDTO apartmentProperty = (ApartmentDTO) object;
		
 		if(apartmentProperty.getUser() == null || 
 				apartmentProperty.getUser().getId() == null)	{
			errors.reject("user not logged in");
		}

		AddressDTO address = apartmentProperty.getAddressSpace();
		if(address == null || StringUtil.isAnyNullOrEmpty(
				address.getPlaceId(), address.getFormattedAddress()))	{
			errors.reject("apartmentAddressHolder", "select the location of property.");
		}
	}
}