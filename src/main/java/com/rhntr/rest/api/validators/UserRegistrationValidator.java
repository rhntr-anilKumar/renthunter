package com.rhntr.rest.api.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.rhntr.application.dto.UserInformationDTO;
import com.rhntr.utils.StringUtil;
import com.rhntr.utils.ValidatorUtil;

@Component
public class UserRegistrationValidator	implements	Validator	{

	@Override
	public boolean supports(Class<?> clazz) {
		return UserInformationDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {

		/* Default validation for null or empty inputs */
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", "phone number is required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailId", "email id is required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passcode", " password is required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPasscode", "confirm passcode is not matching");
		
		UserInformationDTO newUser = (UserInformationDTO) object;

		String emailId = newUser.getEmailId();
		if(!StringUtil.isNullOrEmpty(emailId))		{
			if(!ValidatorUtil.validateEmailId(emailId))	{
				errors.reject("emailId", "invalid email id");
			}
		}
		String phoneNumber = newUser.getPhoneNumber();
		if(!StringUtil.isNullOrEmpty(phoneNumber))	{
			if(!ValidatorUtil.validatePhoneNumber(phoneNumber))	{
				errors.reject("phoneNumber", "invalid phone number");
			}
		}

		String confirmPasscode = newUser.getConfirmPasscode();
		if(!StringUtil.isNullOrEmpty(confirmPasscode))	{
			if(!confirmPasscode.equals(newUser.getPasscode()))	{
				errors.reject("confirmPasscode", "confirm passcode is not matching");
			}
		}
	}
}
