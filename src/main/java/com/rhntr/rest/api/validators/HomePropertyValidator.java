package com.rhntr.rest.api.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.rhntr.application.dto.AddressDTO;
import com.rhntr.application.dto.HomeDTO;
import com.rhntr.utils.StringUtil;

@Component
public class HomePropertyValidator	implements	Validator	{

	@Override
	public boolean supports(Class<?> clazz) {
		return HomeDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {

		/* Default validation for null or empty inputs */
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "enter home name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "constructedOn", "home contruction date required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tenant", "specify type tenants allowed");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bhkLength", "home space in BHK");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "deposit", "home space in BHK");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rent", "home space in BHK");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "agreemantDuriation", "agreement duriation requied");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "buildUpArea", "build up area measument requied");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "negotiable", "negotiable of rent amount is requied");
		
		HomeDTO homeProperty = (HomeDTO) object;
		
 		if(homeProperty.getUser() == null || 
 				homeProperty.getUser().getId() == null)	{
			errors.reject("user not logged in");
		}

		AddressDTO address = homeProperty.getAddressSpace();
		if(address == null || StringUtil.isAnyNullOrEmpty(
				address.getPlaceId(), address.getFormattedAddress()))	{
			errors.reject("homeAddressHolder", "select the location of property.");
		}
	}
}