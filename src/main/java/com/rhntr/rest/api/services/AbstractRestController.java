package com.rhntr.rest.api.services;

/**
 * <b>AbstractRestController	</b> <br/> <br/>
 * 
 * Abstract rest controller provides common functionalities among multiple rest controller's.
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 14-April-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Abstract Controller <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public abstract class AbstractRestController extends AbstractController	{	
	
}
