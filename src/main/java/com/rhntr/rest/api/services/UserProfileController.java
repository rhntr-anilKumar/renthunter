package com.rhntr.rest.api.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.core.enums.PropertyType;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.GoogledAddress;
import com.rhntr.core.model.entities.Home;
import com.rhntr.core.model.entities.PayingGuest;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.AddressRepository;
import com.rhntr.core.model.repositories.GAddressRepository;
import com.rhntr.core.model.repositories.HomeRepository;
import com.rhntr.core.model.repositories.PgRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.RestResponseUtil;

/**
 * 
 * @author Somesh
 *
 */
@RestController
@RequestMapping("/user-profile")
public class UserProfileController extends AbstractController{

	private Logger log = LoggerFactory.getLogger(UserProfileController.class);

	@Autowired
	UserRepository userRepo;

	@Autowired
	AddressRepository addressRepo;

	@Autowired
	GAddressRepository googleAddRepo;

	@Autowired
	HomeRepository homeRepo;

	@Autowired
	PgRepository pgRepo;

	/**
	 * Getting users by user id.
	 * 
	 * @param userId		{@link Long}
	 * @return				{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity getUserById(@PathVariable("id") final Long userId)	throws Exception {

		log.info("Method::getUserById : " + userId);

		if(userId == null)	
			return RestResponseUtil.badResponseEntity("Invalid input : null");

		User user = userRepo.findOne(userId);
		if(user == null)
			return RestResponseUtil.badResponseEntity("User not exists...");

		return RestResponseUtil.responseEntity(user);
	}

	/**
	 * Getting user posted properties by user id.
	 * 
	 * @param userId		{@link Long}
	 * @return              {@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@GetMapping(value="/properties/{id}")
	public ResponseEntity getUserPropertie(@PathVariable("id") final Long userId) throws Exception{

		log.info("Method::gerUserPropertie : " + userId);

		Map<String, Object> response = new HashMap<String, Object>();

		if(userId == null)
			RestResponseUtil.badResponseEntity("Invalid input : null");

		List<Home> homeProperties = new ArrayList<Home>();
		List<PayingGuest> pgProperties = new ArrayList<PayingGuest>();
		Map<Long, GoogledAddress> addressMap = new HashMap<Long, GoogledAddress>();

		if(userRepo.exists(userId))	{

			List<AddressSpace> addressSpaces = addressRepo.getByUserId(userId);

			if(!CollectionUtil.isNullOrEmpty(addressSpaces))	{

				List<Long> pgPropertyIds = new ArrayList<Long>();
				List<Long> homePropertyIds = new ArrayList<Long>();

				addressSpaces.stream().forEach(addressTable -> {

					addressMap.put(addressTable.getGoogledAddress().getId(), addressTable.getGoogledAddress());

					if(PropertyType.HOME.equals(addressTable.getPropertyType()))	{
						homePropertyIds.add(addressTable.getId());
					} else if(PropertyType.PG.equals(addressTable.getPropertyType()))	{
						pgPropertyIds.add(addressTable.getId());
					}
				});

				homeProperties = homeRepo.findAll(homePropertyIds);
				pgProperties = pgRepo.findAll(pgPropertyIds);
			}

		} else {
			return RestResponseUtil.badResponseEntity("User not exists...");
		}

		response.put("homeProperties", homeProperties);
		response.put("pgProperties", pgProperties);
		response.put("addressMap", addressMap);
		return RestResponseUtil.responseEntity(response);
	}

}
