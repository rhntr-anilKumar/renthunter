package com.rhntr.rest.api.services;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.ApartmentDTO;
import com.rhntr.core.model.entities.Apartment;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.ApartmentRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.ApartmentPropertyService;
import com.rhntr.exceptions.BusinessFlowException;
import com.rhntr.rest.api.validators.ApartmentPropertyValidator;
import com.rhntr.utils.RestResponse;
import com.rhntr.utils.RestResponseUtil;

/**
 * <b>ApartmentController	</b> <br/> <br/>
 * 
 * REST end-point for Apartment registration services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 16-DEC-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link RestResponseUtil}, {@link ApartmentRepository}
 * 
 */

@RestController
@RequestMapping("/apartment")
public class ApartmentController extends AbstractController {

	private Logger log = LoggerFactory.getLogger(ApartmentController.class);

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ApartmentPropertyService apartmentPropertyService;
	
	@Autowired
	private ApartmentPropertyValidator apartmentPropertyValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder)	{
		binder.setValidator(apartmentPropertyValidator);
	}
	
	
	@PostMapping(value = "/", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })	
	public ResponseEntity postProperty(@Valid @RequestBody ApartmentDTO apartmentDTO) throws Exception	{
		
		log.debug("METHOD::postProperty(@Valid @RequestBody ApartmentDTO apartmentDTO)");

		User user = apartmentDTO.getUser();

		if(!userRepo.exists(user.getId()))	{
			throw new BusinessFlowException("User not found.");
		} 

		log.info("Apartment property registered for " + apartmentDTO.getName());
		Apartment newProperty = apartmentPropertyService.registeringNewApartmentProperty(apartmentDTO);

		RestResponse response = RestResponse.newInstance(newProperty, apartmentDTO);

		return response.responseEntity(HttpStatus.CREATED);
	}
}