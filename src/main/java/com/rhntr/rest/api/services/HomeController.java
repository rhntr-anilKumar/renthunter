package com.rhntr.rest.api.services;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.HomeDTO;
import com.rhntr.core.model.entities.Home;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.AddressRepository;
import com.rhntr.core.model.repositories.HomeRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.HomePropertyService;
import com.rhntr.exceptions.BusinessFlowException;
import com.rhntr.rest.api.validators.HomePropertyValidator;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.RestResponse;
import com.rhntr.utils.RestResponseUtil;

/**
 * <b>HomeController		</b> <br/> <br/>
 * 
 * REST end-point for Home registration services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 04-May-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Madhu
 * @see			{@link RestResponseUtil}, {@link HomeRepository}
 * 
 */

@RestController
@RequestMapping("/home")
public class HomeController extends AbstractRestController{

	
	private Logger log = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private HomeRepository homeRepository;
	
	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private HomePropertyService homePropertyService;

	@Autowired
	private HomePropertyValidator homePropertyValidator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder)	{
		binder.setValidator(homePropertyValidator);
	}
	
	/**
	 *  Getting All Home Details
	 *  
	 *  @return {@link ResponseEntity}
	 *  
	 *  @throws Exception
	 */
	
	
	@GetMapping("/")
	public ResponseEntity getAllHome(  ) throws Exception
	{
		log.info("Method::getAllHome : ");
		
		List<Home> home = homeRepository.findAll();
		
		if(CollectionUtil.isAnyNullOrEmpty(home)) {
			return RestResponseUtil.badResponseEntity("Home Details Not found");
		}
	
		return RestResponseUtil.responseEntity(home);
	}
	
	/**
	 * Get All Home Details By ID
	 * 
	 * @return {@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	
	@GetMapping("/{id}")
	public ResponseEntity getHome(@PathVariable("id") final Long HomeId) throws Exception
	{
	
		log.info(" Method : getHome " + HomeId);
		
		if(HomeId == null)
			return RestResponseUtil.badResponseEntity("Invalid Input :  null");
		
		Home home = homeRepository.findOne(HomeId);
		
		if(home == null) 
			return RestResponseUtil.badResponseEntity(" No home details found");
		
		return RestResponseUtil.responseEntity(home);
	}
	
	@PostMapping(value = "/", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })	
	public ResponseEntity postProperty(@Valid @RequestBody HomeDTO homeDTO) throws Exception	{
		
		log.debug("METHOD::postProperty(@RequestBody HomeDTO home)");

		User user = homeDTO.getUser();
		log.info("image list : " + homeDTO.getHomePictures());
		if(!userRepo.exists(user.getId()))	{
			throw new BusinessFlowException("User not found.");
		} 

		log.info("Home property registered for " + homeDTO.getName());
		Home newProperty = homePropertyService.registeringNewHomeProperty(homeDTO);

		RestResponse response = RestResponse.newInstance(newProperty, homeDTO);

		return response.responseEntity(HttpStatus.CREATED);
	}
}