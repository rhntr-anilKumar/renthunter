package com.rhntr.rest.api.services;

/**
 * <b>AbstractController	</b> <br/> <br/>
 * 
 * Abstract controller provides common functionalities among multiple controller's.
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 14-April-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Abstract Controller <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public abstract class AbstractController	{

}