package com.rhntr.rest.api.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.ApartmentDTO;
import com.rhntr.application.dto.HomeDTO;
import com.rhntr.core.services.ApartmentPropertyService;
import com.rhntr.core.services.HomePropertyService;
import com.rhntr.utils.RestResponseUtil;

/**
 * <b>StartupLoadsController	</b> <br/> <br/>
 * 
 * REST end-point for some load on pop-up/data filling services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 01-Jan-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
@RestController
@RequestMapping("/load")
public class StartupLoadsController {
	
 	private Logger log = LoggerFactory.getLogger(this.getClass());

 	@Autowired
 	private HomePropertyService homePropertyService;
 	
 	@Autowired
 	private ApartmentPropertyService apartmentPropertyService;
 	
 	@GetMapping(value="/recent-properties")
 	public ResponseEntity recientPostedProperties()	throws Exception	{
 		
 		log.debug("public ResponseEntity recientPostedProperties()	throws Exception");
 		
 		List<HomeDTO> homes = homePropertyService.recentPostedProperty();
 		List<ApartmentDTO> apartmens = apartmentPropertyService.recentPostedProperty();
 		Map<String, Object> properties = new HashMap<String, Object>();
 		properties.put("homes", homes);
 		properties.put("apartmens", apartmens);
 		return RestResponseUtil.responseEntity(properties);
 	}
}