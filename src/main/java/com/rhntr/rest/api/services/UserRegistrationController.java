package com.rhntr.rest.api.services;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.UserInformationDTO;
import com.rhntr.core.Constants;
import com.rhntr.core.Params;
import com.rhntr.core.application.security.jwt.JwtTokenUtil;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.UserMutatorService;
import com.rhntr.exceptions.BusinessFlowException;
import com.rhntr.rest.api.validators.UserRegistrationValidator;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.RestResponseUtil;
import com.rhntr.utils.StringUtil;

/**
 * <b>UserRegistrationController		</b> <br/> <br/>
 * 
 * REST end-point for rentee/owner registration services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 25-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link RestResponseUtil}, {@link UserMutatorService}
 * 
 */
@RestController
@RequestMapping("/user")
public class UserRegistrationController	extends AbstractRestController	{

	private Logger log = LoggerFactory.getLogger(UserRegistrationController.class);

	@Autowired
	private UserMutatorService userMutatorService;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	@Autowired
	private UserRegistrationValidator userRegistrationValidator;

	@InitBinder
	private void initBinder(WebDataBinder binder)	{
		binder.setValidator(userRegistrationValidator);
	}
	
	/**
	 * Getting application all users.
	 * 
	 * @return	{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@GetMapping(value = "/")
	public ResponseEntity getAllUser(	)	throws Exception {

		log.info("Method::getAllUser : ");
		
		List<User> user = userRepository.findAll();
		if(CollectionUtil.isNullOrEmpty(user))
			return RestResponseUtil.badResponseEntity("User's not found...");

		return RestResponseUtil.responseEntity(user);
	}
	
	
	/**
	 * Getting users by user id.
	 * 
	 * @param userId		{@link Long}
	 * @return				{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity getUser(@PathVariable("id") final Long userId)	throws Exception {

		log.info("Method::getUser : " + userId);
		
		if(userId == null)	
			return RestResponseUtil.badResponseEntity("Invalid input : null");
		
		User user = userRepository.findOne(userId);
		if(user == null)
			return RestResponseUtil.badResponseEntity("User not exists...");

		return RestResponseUtil.responseEntity(user);
	}

	/**
	 * Modifying existing user information.
	 * 
	 * @param userId		{@link Long}
	 * @param userInfo		{@link UserInformationDTO}
	 * @return				{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity modifyUserDetails(@PathVariable("id") Long userId, 
			@Validated @RequestBody UserInformationDTO userInfo) throws Exception{

		log.info("Method::modifyUserDetails : " + userId);

		userInfo.setId(userId);
	
		userInfo = userMutatorService.modifyUserDetails(userInfo);
		
		User user = (User) userInfo.get(Params.USER_OBJECT);
		
		return RestResponseUtil.responseEntity(user);
	}

	/**
	 * Deleting user entry from application.
	 * 
	 * @param userId		{@link Long}
	 * @return				{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity deleteUserEntry(@PathVariable("id") Long userId) throws Exception{
		
		//throw new BusinessFlowException("Unsupported operation.");
		
		if(userId == null)	
			return RestResponseUtil.badResponseEntity("Invalid input : null");
		
		User user = userRepository.findOne(userId);
		if(user == null)
			return RestResponseUtil.badResponseEntity("User not exists...");

		// deletes the user entry
		userRepository.delete(user);
		
		return RestResponseUtil.responseEntity(user.getFirstName() + " deleted successfully.");
	}

	/**
	 * Deleting all users entry from application.
	 * 
	 * @return	{@link ResponseEntity}
	 * 
	 * @throws 	Exception
	 */
	@DeleteMapping(value = "/")
	public ResponseEntity deleteAllUserEntry(	) throws Exception{
		
		//throw new BusinessFlowException("Unsupported operation.");
		
		// check for entry are there or not.
		if(Constants.EMPTY_COUNT_L.equals(userRepository.count()))
			return RestResponseUtil.badResponseEntity("No entries are there to delete.");

		// now delete existing all entries.
		userRepository.deleteAll();
		
		if(Constants.EMPTY_COUNT_L.equals(userRepository.count()))
			return RestResponseUtil.responseEntity("All reccord entries are deleted.");
		else
			return RestResponseUtil.badResponseEntity("There is problem while deleting entries.");
	}

    /**
     * Adding user and user information.
     * 
     * @param newUser		{@link UserInformationDTO}
     * @return				{@link ResponseEntity}
     * 
     * @throws Exception
     */
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity userRegisterationBySignUp(@Valid @RequestBody UserInformationDTO newUser) throws Exception	{

		log.info("Registering new user : " + newUser.getPhoneNumber());

		newUser = userMutatorService.registerNewUser(newUser);
		
		return RestResponseUtil.responseEntity(newUser.get(Params.USER_OBJECT), HttpStatus.CREATED);
	}
	
	/**
     * Authenticating user for sign in.
	 * 
	 * @param userDto		{@link UserInformationDTO}
	 * @return				{@link ResponseEntity}
	 * @throws Exception
	 */
	@PostMapping(value = "/authenticate", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity authenticateUser(@RequestBody UserInformationDTO userDto) throws Exception	{

		log.info("Authenticate User " + userDto.getPhoneNumber() + " : " + userDto.getPasscode());
		if(!StringUtil.isNullOrEmpty(userDto.getEmailId()) 
				|| !StringUtil.isNullOrEmpty(userDto.getPhoneNumber()))	{
			
			String identity = userDto.getEmailId();
			if(StringUtil.isNullOrEmpty(identity))	{
				identity = userDto.getPhoneNumber();
			}

			User user = userRepository.findByEmailIdOrPhoneNumber(userDto.getEmailId(), userDto.getPhoneNumber());
			if(user == null || user.getPasscode().equals(
					bcryptEncoder.encode(userDto.getPasscode())))	{
				throw new BusinessFlowException("User Don't Exist.");
			}

			return RestResponseUtil.responseEntity(user);
			/*final Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
						identity, userDto.getPasscode())
				);
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
			final User user = userRepository.findByEmailIdOrPhoneNumber(
					userDto.getEmailId(), userDto.getPhoneNumber());
			final String token = jwtTokenUtil.generateToken(user);
			return RestResponseUtil.responseEntity(token);*/
		}
		return RestResponseUtil.badResponseEntity("enter valid email id/phone number");
	}
}