package com.rhntr.rest.api.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.AddressDTO;
import com.rhntr.core.Constants;
import com.rhntr.core.Params;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.repositories.AddressRepository;
import com.rhntr.core.services.AddressMutatorService;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.RestResponseUtil;

/**
 * <b>AddressRegistrationController		</b> <br/> <br/>
 * 
 * REST end-point for address registration services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 25-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link RestResponseUtil}, {@link AddressMutatorService}
 * 
 */

@RestController
@RequestMapping("/address")
public class AddressRegistrationController extends AbstractRestController	{

	private Logger log = LoggerFactory.getLogger(AddressRegistrationController.class);

	@Autowired
	private AddressMutatorService addressMutatorService; 

	@Autowired
	private AddressRepository addressRepository;

	/**
	 * Getting application all users addresses.
	 * 
	 * @return		{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@GetMapping(value = "/")
	public ResponseEntity getAddressSpace(	) throws Exception	{

		log.info("Method::getAddressSpace all ");
		
		List<AddressSpace> address = addressRepository.findAll();
		if(CollectionUtil.isNullOrEmpty(address))
			RestResponseUtil.badResponseEntity("Address are not found.");
		
		return RestResponseUtil.responseEntity(address);
	}

	/**
	 * Getting user address by address id.
	 * 
	 * @param addressId		{@link Long}
	 * @return				{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity getAddressSpaceById(@PathVariable("id") final Long addressId) throws Exception	{

		log.info("Method::getAddressSpaceById addressId @" + addressId);
		if(addressId == null)
			return RestResponseUtil.badResponseEntity(
					"invalid input : address id can't be null or empty...");

		AddressSpace address = addressRepository.findOne(addressId);
		if(address == null)
			return RestResponseUtil.badResponseEntity("Address not found.");

		log.info("Address found for user : " + address.getUser().getFirstName());
		
		return RestResponseUtil.responseEntity(address);
	}

	/**
	 * Adding user address in application.
	 * 
	 * @param addressInfo		{@link AddressDTO}
	 * @return					{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@PostMapping(value = "/", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity registerAddressSpace(@RequestBody AddressDTO addressInfo)	throws Exception	{
		
		log.info("Method::registerAddressSpace userId : " + addressInfo.getUserId());
		log.info("Property Type : " + addressInfo.getPropertyType());
		addressInfo = addressMutatorService.addUserAddress(addressInfo);
	
		if(addressInfo.hasErrors())	{
			return RestResponseUtil.badResponseEntity(addressInfo);
		}
		
		AddressSpace userAddress = (AddressSpace) 
				addressInfo.get(Params.USER_ADDRESS_OBJECT);
		return RestResponseUtil.responseEntity(userAddress);
	}

	/**
	 * Modifying existing user address information.
	 * 
	 * @param id			{@link Long}	
	 * @param addressInfo	{@link AddressDTO}
	 * @return				{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@PutMapping(value = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity modifyUserAddressDetails(@PathVariable("id") Long id,
							@RequestBody AddressDTO addressInfo)	throws Exception	{
		
		log.info("Method::modifyUserAddressDetails userId : " + addressInfo.getUserId());
		// setting modifying address id
		addressInfo.setId(id);
		
		addressInfo = addressMutatorService.modifyUserAddressDetails(addressInfo);
	
		if(addressInfo.hasErrors())	{
			return RestResponseUtil.badResponseEntity(addressInfo);
		}
		
		AddressSpace userAddress = (AddressSpace) addressInfo.get(Params.USER_ADDRESS_OBJECT);
		
		return RestResponseUtil.responseEntity(userAddress);
	}

	/**
	 * Deleting all address entries from the application.
	 * 
	 * @return	{@link ResponseEntity}
	 * @throws 	Exception
	 */
	@DeleteMapping(value = "/")
	public ResponseEntity deleteAllAddressEntries(	)	throws Exception	{
		
		// check for entry are there or not.
		if(Constants.EMPTY_COUNT_L.equals(addressRepository.count()))
			return RestResponseUtil.badResponseEntity("No entries are there to delete.");

		// now delete existing all entries.
		addressRepository.deleteAll();
		
		if(Constants.EMPTY_COUNT_L.equals(addressRepository.count()))
			return RestResponseUtil.responseEntity("All reccord entries are deleted.");
		else
			return RestResponseUtil.badResponseEntity("There is problem while deleting entries.");
	}

	/**
	 * Deleting specified address entries from the application.
	 * 
	 * 
	 * @param addressSpaceId	{@link Long}
	 * @return					{@link ResponseEntity}
	 * 
	 * @throws Exception
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity deleteAddressEntryById(@PathVariable("id") Long addressSpaceId)	throws Exception	{
		
		if(addressSpaceId == null)	{
			return RestResponseUtil.badResponseEntity("Address id can't be null.");
		}
		
		AddressSpace addressSpace = addressRepository.findOne(addressSpaceId);
		
		if(addressSpace == null)	{
			return RestResponseUtil.badResponseEntity("Address doesn't exist for the given id.");
		}
		
		// remove the entry.
		addressRepository.delete(addressSpace);
		
		return RestResponseUtil.responseEntity("Address of id " + addressSpaceId + " has been removed successfully.");		
	}
}