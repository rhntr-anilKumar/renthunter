package com.rhntr.rest.api.services;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.AddressDTO;
import com.rhntr.application.dto.PgDTO;
import com.rhntr.core.model.entities.PayingGuest;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.PgRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.PgPropertyServices;
import com.rhntr.exceptions.BusinessFlowException;
import com.rhntr.rest.api.validators.HomePropertyValidator;
import com.rhntr.rest.api.validators.PGPropertyValidator;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.RestResponse;
import com.rhntr.utils.RestResponseUtil;
import com.rhntr.utils.StringUtil;

/**
 * <b>PayingGuestController		</b> <br/> <br/>
 * 
 * REST end-point for paying guest property registration services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 04-May-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link RestResponseUtil}
 * 
 */
@RestController
@RequestMapping("/paying-guest")
public class PayingGuestController	extends AbstractRestController {

	private Logger log = LoggerFactory.getLogger(PayingGuestController.class);

	@Autowired
	private PgRepository pgRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private PgPropertyServices pgPropertyServices;

	@Autowired
	private PGPropertyValidator pgPropertyValidator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder)	{
		binder.setValidator(pgPropertyValidator);
	}

	/**
	 * Returns all PG properties.
	 * 
	 * @return
	 */
	@GetMapping(value = "/")
	public ResponseEntity getAll()	{
		log.info("Get all PG's list : ");
		List<PayingGuest> pgs = pgRepo.findAll();
		if(pgs == null)
			pgs = CollectionUtil.EMPTY_LIST;
		return RestResponseUtil.responseEntity(pgs);
	}

	/**
	 * Returns requested PG property by id.
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity get(@PathVariable("id") Long id)	{
		log.info("Get PG's info : " + id);
		PayingGuest pg = pgRepo.findOne(id);
		if(pg == null)
			return RestResponseUtil.badResponseEntity("pg details not found");
		return RestResponseUtil.responseEntity(pg);
	}

	/**
	 * Registers new Pg property and Address of pg if not exists.
	 * 
	 * @param pgDTO
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity postProperty(@Valid @RequestBody PgDTO pgDTO)	throws Exception	{

		log.debug("METHOD::postingProperty(@RequestBody PgDTO pgDTO)");

		AddressDTO address = pgDTO.getAddressSpace();
		if(address == null || StringUtil.isAnyNullOrEmpty(
				address.getPlaceId(), address.getFormattedAddress()))	{
			throw new BusinessFlowException("Paying guest address is not selected.");
		}

		User user = pgDTO.getUser();
		if(user == null || user.getId() == null)	{
			throw new BusinessFlowException("Session expired.");
		} else {
			if(!userRepo.exists(user.getId()))	{
				throw new BusinessFlowException("User not found.");
			}
		}

		log.info("Property PG registered for " + pgDTO.getType());
		PayingGuest newProperty = pgPropertyServices.registeringNewPgProperty(pgDTO);

		RestResponse response = RestResponse.newInstance(newProperty, pgDTO);

		return response.responseEntity(HttpStatus.CREATED);
	}
}