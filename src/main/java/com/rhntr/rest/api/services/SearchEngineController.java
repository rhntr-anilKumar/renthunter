package com.rhntr.rest.api.services;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.AreaMap;
import com.rhntr.application.dto.AreaSearchInput;
import com.rhntr.core.Params;
import com.rhntr.core.services.SearchEngineService;
import com.rhntr.utils.RestResponse;
import com.rhntr.utils.RestResponseUtil;

/**
 * <b>SearchEngineController	</b> <br/> <br/>
 * 
 * REST end-point for search/location navigation services.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 25-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> REST Controller/REST end-point. <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
@RestController
@RequestMapping("/search")
public class SearchEngineController {

	private Logger log = LoggerFactory.getLogger(SearchEngineController.class);

	@Autowired
	private SearchEngineService searchService;

	/**
	 * @param searchInput
	 * @return
	 */
	@PostMapping(value = "/q", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity searchSpace(@RequestBody AreaSearchInput searchInput)	{
		
		Map map = searchService.searchAddressSpace(searchInput);

		RestResponse response = RestResponse.newInstance(map.get(Params.PROPERTIES), searchInput);
		response.set(Params.ADDRESS_MAP, map.get(Params.ADDRESS_MAP));
		response.set(Params.GOOGLE_ADDRESS_IDS, map.get(Params.GOOGLE_ADDRESS_IDS));
		
		return response.responseEntity(HttpStatus.OK);
	}
}