package com.rhntr.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.rhntr.application.dto.AbstractDTO;


/**
 * <b>RestResponse	</b> <br/> <br/>
 * 
 * Rest Response class is used for sending response to request in json format.
 * Rest Response contains data, input and additional parameter appended to 'get' property key.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 06-OCT-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link ServletResponse}, {@link HttpServletResponse}
 * 
 */
public class RestResponse	{

	Map<String, Object> entries = new HashMap<String, Object>();

	private Object data = null;

	private Object input = null;

	/**
	 * Default constructor, not accessible directly.
	 */
	private RestResponse()	{
		
	}

	/**
	 * Creates new instance of RestResponse object.
	 * 
	 * @return
	 */
	public static RestResponse newInstance()	{
		return new RestResponse();
	}

	/**
	 * Creates new instance with response body/data.
	 * 
	 * @param response
	 * @return
	 */
	public static RestResponse newInstance(Object response)	{
		RestResponse restResponse = new RestResponse();
		restResponse.setResponse(response);
		return restResponse;
	}

	/**
	 * Creates new instance with response body/data and input request.
	 * 
	 * @param response
	 * @param input
	 * @return
	 */
	public static RestResponse newInstance(Object response, Object input)	{
		RestResponse restResponse = RestResponse.newInstance(response);
		restResponse.setInput(input);
		return restResponse;
	}

	/**
	 * Returns additional added properties as appended to 'get' property.
	 * @return
	 */
	public Map getGet() {
		return entries;
	}

	/**
	 * Sets the additional information to response entity.
	 * 
	 * @param key
	 * @param val
	 * @return
	 */
	public Object set(String key, Object val)	{
		Object prevVal = null;
		if(entries.containsKey(key))	{
			prevVal = entries.get(key);
		}
		entries.put(key, val);
		return prevVal;
	}

	/**
	 * Sets the response body to the rest response entity.
	 * 
	 * @param response
	 */
	public void setResponse(Object response) {
		this.data = response;
	}

	/**
	 * Response data
	 * 
	 * @return
	 */
	public Object getData() {
		return data;
	}
	
	/**
	 * Response with request input, if want in response back.
	 * 
	 * @param input
	 */
	public void setInput(Object input) {
		if(input instanceof AbstractDTO)	{
			/* Removing addition data map entries from dto, 
			   which added while processing request */
			ReflectionUtil.changeFieldValue(input, "dataMap", null);
		}
		this.input = input;
	}
	
	public Object getInput() {
		return input;
	}

	/**
	 * Direct call from rest response object to create response entity.
	 * 
	 * @return
	 */
	public ResponseEntity responseEntity() {
		return RestResponseUtil.responseEntity(this);
	}

	/**
	 * Direct call from rest response object to create response entity with status.
	 * 
	 * @param httpStatus
	 * @return
	 */
	public ResponseEntity responseEntity(HttpStatus httpStatus) {
		return RestResponseUtil.responseEntity(this, httpStatus);
	}
}
