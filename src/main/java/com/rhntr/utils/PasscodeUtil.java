package com.rhntr.utils;

/**
 * <b>PasscodeUtil	</b> <br/> <br/>
 * 
 * Passcode utility class is used for securing credentials/password 
 * and other important information.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Utility Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class PasscodeUtil {

	private PasscodeUtil() {
		throw new UnsupportedOperationException();
	}
	
	public static String getHashPasscode(String originalPasscode)	{
		return new String(originalPasscode.getBytes());
	}
}
