package com.rhntr.utils;

/**
 * <b>ValidatorUtil		</b> <br/> <br/>
 * 
 * Validator utility class for handling common validation operations.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 14-April-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Utility Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class ValidatorUtil {
	
	private static final String EMAIL_PATTERN = "^[A-Za-z0-9+_.-]+@(.+)$";
	
	private static final String PHONE_NUMBER_SIMPLE_PATTERN = "\\d{10}";

	/**
	 * Email validator
	 * 
	 * @param emailId
	 * @return
	 */
	public static boolean validateEmailId(final String emailId)	{
		return emailId.matches(EMAIL_PATTERN);
	}

	/**
	 * Phone validator
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static boolean validatePhoneNumber(final String phoneNumber) {
		return phoneNumber.matches(PHONE_NUMBER_SIMPLE_PATTERN);
	}
}