package com.rhntr.utils;

import java.util.Date;

public class DateAndTimeUtil {

	/**
	 * @return current date 
	 */
	public static Date currentDate()	{
		return new Date(System.currentTimeMillis());
	}
}
