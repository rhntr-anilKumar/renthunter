package com.rhntr.utils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * <b>RequestUtil	</b> <br/> <br/>
 * 
 * Request utility class is used for handling special operation 
 * on request param's and manipulating request objects.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Utility Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link ServletRequest}, {@link HttpServletRequest}
 * 
 */
public class RequestUtil {

	private RequestUtil() {
		throw new UnsupportedOperationException();
	}
}