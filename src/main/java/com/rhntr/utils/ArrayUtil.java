package com.rhntr.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <b>ArrayUtil	</b> <br/> <br/>
 * 
 * Array utility class is used for handling string common operation.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 07-Aug-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Utility Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see 		{@link ArrayUtil}
 * 
 */

public class ArrayUtil {

	/**
	 * 
	 */
	private ArrayUtil() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * @param arr
	 * @return
	 */
	public static boolean isNullOrEmpty(Object[] arr)	{
		if(arr == null || arr.length == 0)
			return true;
		return false;
	}

	/**
	 * @param array
	 * @return
	 */
	public static List toList(Object[] array)	{
		return ArrayUtil.toList(array, new ArrayList());
	}

	/**
	 * @param array
	 * @param allowDuplicateEntries
	 * @return
	 */
	public static List toList(Object[] array, boolean allowDuplicateEntries)	{
		return ArrayUtil.toList(array, new ArrayList(), allowDuplicateEntries);
	}

	/**
	 * @param array
	 * @param existingList
	 * @return
	 */
	public static List toList(Object[] array, List existingList)	{
		return ArrayUtil.toList(array, existingList, Boolean.TRUE);
	}

	/**
	 * @param array
	 * @param existingList
	 * @param allowDuplicateEntries
	 * @return
	 */
	public static List toList(Object[] array, List existingList, boolean allowDuplicateEntries)	{
		
		if(!ArrayUtil.isNullOrEmpty(array))	{
			if(existingList == null)	existingList = new ArrayList();
			for(Object obj : array)	{
				if(obj != null)	{
					if(allowDuplicateEntries || !existingList.contains(obj))
						existingList.add(obj);
				}
			}
		}
		return CollectionUtil.isNullOrEmpty(existingList) ? null : existingList;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Set toSet(Object[] array)	{
		return ArrayUtil.toSet(array, new HashSet());
	}

	/**
	 * @param array
	 * @param existingSet
	 * @return
	 */
	public static Set toSet(Object[] array, Set existingSet)	{
		
		if(!ArrayUtil.isNullOrEmpty(array))	{
			if(existingSet == null)	existingSet = new HashSet();
			for(Object obj : array)	{
				if(obj != null)	{
					existingSet.add(obj);
				}
			}
		}
		return CollectionUtil.isNullOrEmpty(existingSet) ? null : existingSet;
	}
}