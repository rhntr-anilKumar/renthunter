package com.rhntr.practice.area;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.AreaMap;
import com.rhntr.application.dto.AreaSearchInput;
import com.rhntr.core.Params;
import com.rhntr.core.model.entities.SearchArea;
import com.rhntr.core.model.entities.User;
import com.rhntr.utils.CollectionUtil;

@Service
@Transactional
public class SearchEngineServiceImp2 implements SearchEngineService{

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SearchEngineDao searchEngineDao;

	@Override
	public AreaMap search(AreaSearchInput searchInput) {

		log.debug("Method::search(AreaSearchInput searchInput)");
		
		List<SearchArea> areas = searchEngineDao.searchEngine(searchInput);

		AreaMap areaMap = new AreaMap();

		Set<User> users = new HashSet<User>();

		/*if(!CollectionUtil.isNullOrEmpty(areas)){
			areas.stream().forEach(obj -> {
				users.add(obj.getUser());
			});
		}*/
		return areaMap;
	}

	@Override
	public AreaMap saveArea(final String name) {
		// TODO Auto-generated method stub
		try {
			return searchEngineDao.saveUserData(name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public AreaMap allData() {
		AreaMap areaMap = searchEngineDao.allData();
		List<SearchArea> areas = (List<SearchArea>)
					areaMap.get("map");
		log.info("processing users");
		Set<User> users = new HashSet<User>();///areas.stream().map(user -> user.getUser()).collect(Collectors.toSet());
		areaMap.set(Params.USER_OBJECT, users);
		return areaMap;
	}
}
