package com.rhntr.practice.area;

import java.util.List;
import java.util.Map;

import com.rhntr.application.dto.AreaMap;
import com.rhntr.application.dto.AreaSearchInput;
import com.rhntr.core.model.entities.SearchArea;

public interface SearchEngineDao {

	public List<SearchArea> searchEngine(AreaSearchInput searchInput);

	public AreaMap saveArea(final String name) throws Exception;

	public AreaMap allData();

	public AreaMap saveUserData(String name);
}