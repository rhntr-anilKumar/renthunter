package com.rhntr.practice.area;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rhntr.application.dto.AreaMap;
import com.rhntr.application.dto.AreaSearchInput;
import com.rhntr.core.Constants;
import com.rhntr.core.model.entities.PayingGuest;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.PgRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.RestResponseUtil;

@RestController
@RequestMapping("/practice")
public class RestPracticeController {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SearchEngineService searchService;
	
	@Autowired
	private PgRepository pgRepository;

	/**
	 * @return
	 */
	@GetMapping("/welcome")
	public ResponseEntity getContacts() {
		System.out.println("Reading Data..................");
		log.info("Code is executing");		
		return RestResponseUtil.responseEntity("Code is executing", HttpStatus.OK);
	}
	
	/**
	 * Searching service end point for input value from text box,
	 * <br/>'i' is stand for input from text-box and 'q' for input query.
	 * 
	 * @param inputString
	 * @return
	 */
	@GetMapping("/q/i/{input}")
	public ResponseEntity<AreaMap> search(@PathVariable("input") String inputString)	{
		
		log.info("SearchEngineController :: searching..............");

		AreaSearchInput searchInput = new AreaSearchInput();
		searchInput.setInputSource(Constants.INPUT_TYPE_TEXT_BOX);
		searchInput.setSearchInput(inputString);
		AreaMap areaMap = searchService.search(searchInput);
		
		return RestResponseUtil.responseEntity(areaMap, HttpStatus.OK);
	}

	@GetMapping("/all")
	public ResponseEntity<AreaMap> getAllData()	{
		log.info("SearchEngineController :: searching getting all data..............");
		AreaMap areaMap = searchService.allData();
		return RestResponseUtil.responseEntity(areaMap, HttpStatus.OK);
	}

	/**
	 * Searching service end point for input value from google map,
	 * <br/>'m' is stand for input from map and 'q' for input query.
	 * 
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	@GetMapping("/save/{name}")
	public ResponseEntity<AreaMap> save(@PathVariable("name") String name)	{
		AreaMap areaMap = searchService.saveArea(name);
		return RestResponseUtil.responseEntity(areaMap, HttpStatus.OK);
	}
	

	@GetMapping(value = "/pg")
	public ResponseEntity getAllPg(	)	throws Exception {

		log.info("Method::getAllPg : ");
		
		List<PayingGuest> pg = pgRepository.findAll();
		if(CollectionUtil.isNullOrEmpty(pg))
			return RestResponseUtil.badResponseEntity("pg not found...");

		return RestResponseUtil.responseEntity(pg);
	}
	
	@PostMapping(value="/pg")
	public ResponseEntity setPg(@RequestBody PayingGuest pg)throws Exception {

		log.info("Method::setPg : ");
		
		pg=pgRepository.save(pg);

		if(pg==null)
			return RestResponseUtil.badResponseEntity("pg not found...");
		else
			return RestResponseUtil.responseEntity(pg);

	}
}