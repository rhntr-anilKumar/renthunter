package com.rhntr.practice.area;


import com.rhntr.application.dto.AreaMap;
import com.rhntr.application.dto.AreaSearchInput;

public interface SearchEngineService {

	public AreaMap search(AreaSearchInput searchInput);
	
	public AreaMap saveArea(final String name);
	
	public AreaMap allData();
}
