package com.rhntr.practice.area;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rhntr.application.dto.AreaMap;
import com.rhntr.application.dto.AreaSearchInput;
import com.rhntr.core.Constants;
import com.rhntr.core.enums.PropertyType;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.GoogledAddress;
import com.rhntr.core.model.entities.SearchArea;
import com.rhntr.core.model.entities.User;
import com.rhntr.utils.StringUtil;

@Repository
public class SearchEngineDaoImp implements SearchEngineDao {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<SearchArea> searchEngine(AreaSearchInput searchInput){
		
		AreaMap areaMap = new AreaMap();
		Map<String, Object> map = new HashMap<String, Object>();

		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(SearchArea.class);
		
		Restrictions.and(Restrictions.ilike("", ""), Restrictions.ilike("", ""), Restrictions.ilike("", ""), Restrictions.ilike("", ""));

		if(Constants.INPUT_TYPE_MAP.equals(searchInput.getInputSource())){			
			criteria.add(Restrictions.eq("latitude", searchInput.getLatitude()));
			criteria.add(Restrictions.eq("longitude", searchInput.getLongitude()));

		}else if(!StringUtil.isNullOrEmpty(searchInput.getSearchInput())){
			
			Disjunction orStatements = Restrictions.disjunction();
			
			orStatements.add(Restrictions.ilike("siteAddress", searchInput.getSearchInput(), MatchMode.ANYWHERE));
			orStatements.add(Restrictions.ilike("addOnServices", searchInput.getSearchInput(), MatchMode.ANYWHERE));

			if(searchInput.getInputSource().contains("with food"))	{
				orStatements.add(Restrictions.eq("food_service", true));
			}
			if(searchInput.getInputSource().contains("without food") )	{
				orStatements.add(Restrictions.eq("foodService", false));				
			}
			if(StringUtil.isContains(searchInput.getSearchInput(), "girl", "girl pg", "girls", "girls pg"))	{
				orStatements.add(Restrictions.eq("typeOfPG", Constants.PG_TYPE_GIRLS));
			}else if(StringUtil.isContains(searchInput.getSearchInput(), "boy", "boy pg", "boys", "boys pg")){
				orStatements.add(Restrictions.eq("typeOfPG", Constants.PG_TYPE_BOYS));
			}else if(searchInput.getSearchInput().contains("pg"))	{
				orStatements.add(Restrictions.eq("typeOfPG", Constants.PG_TYPE_GIRLS));
				orStatements.add(Restrictions.eq("typeOfPG", Constants.PG_TYPE_BOYS));
			}
			criteria.add(orStatements);
		}
		
		List<SearchArea> areas = criteria.list();

		return areas;
	}

	public AreaMap saveArea(final String name) throws Exception {

		AreaMap areaMap = new AreaMap();
		Map<String, Object> map = new HashMap<String, Object>();

		Session session = this.sessionFactory.getCurrentSession();
		
		SearchArea searchArea = new SearchArea();
		searchArea.setAddOnServices("AC "+ name);
		searchArea.setFoodService(true);
		searchArea.setLatitude(73.5869d);
		searchArea.setLongitude(-58.25668d);
		searchArea.setSiteAddress("Some Address");
		searchArea.setSiteType("PG");
		searchArea.setTypeOfPG('G');

		session.save(searchArea);
		session.flush();

		Criteria criteria = session.createCriteria(SearchArea.class);
		List<SearchArea> areas = criteria.list();
		map.put("areaMap", areas);

		User newUser = new User();
		newUser.setFirstName("Raj 23");
		newUser.setLastName("Patil123");
		newUser.setPasscode("234234");
		newUser.setDateOfBirth(new Date());
		session.saveOrUpdate(newUser);
		session.flush();

		criteria = session.createCriteria(User.class);
		List<User> users = criteria.list();
		map.put("usersMap", users);

		areaMap.set("map", map);
		return areaMap;
	}

	@Override
	public AreaMap allData() {
		Session session = sessionFactory.getCurrentSession();		
		Map<String, Object> map = new HashMap<String, Object>();
		
		Criteria criteria = session.createCriteria(SearchArea.class);
		List<SearchArea> list1 = (List<SearchArea>)criteria.list();
		map.put("searchArea", list1);
		
		/*criteria = session.createCriteria(User.class);
		List<User> list2 = (List<User>)criteria.list();
		map.put("Users", list2);*/

		AreaMap areaMap = new AreaMap();
		areaMap.set("map", map);

		return areaMap;
	}

	@Override
	public AreaMap saveUserData(String name) {

		AreaMap areaMap = new AreaMap();
		Map<String, Object> map = new HashMap<String, Object>();

		Session session = this.sessionFactory.getCurrentSession();

		
		User newUser = new User();
		newUser.setFirstName("Raj 23");
		newUser.setLastName("Patil123");
		newUser.setPasscode("234234");
		newUser.setDateOfBirth(new Date());
		newUser.setEmailId("anilkumar@gmail.com");
		newUser.setPhoneNumber("906544890");
		session.saveOrUpdate(newUser);
		session.flush();

		AddressSpace space = new AddressSpace();
		GoogledAddress googledAddress = new GoogledAddress();
		googledAddress.setAdministrativeAreaLevel2("Gulbarga");
		googledAddress.setLocality("Gulbarga");
		googledAddress.setCountry("INDIAN");
		googledAddress.setAdministrativeAreaLevel1("KARNATAKA");
		googledAddress.setPostalCode("585103");
		space.setGoogledAddress(googledAddress);
		space.setUser(newUser);
		space.setPropertyType(PropertyType.HOME);
		session.save(space);
		session.flush();

		space = new AddressSpace();
		googledAddress = new GoogledAddress();
		space.setPropertyType(PropertyType.PG);
		googledAddress.setAdministrativeAreaLevel2("Bangalore");
		googledAddress.setAdministrativeAreaLevel1("KARNATAKA");
		googledAddress.setCountry("INDIAN");
		googledAddress.setPostalCode("585103");
		space.setUser(newUser);
		session.save(space);
		session.flush();

		Criteria criteria = session.createCriteria(User.class);
		List<User> users = criteria.list();
		users.forEach(user -> {
			Hibernate.initialize(user.getAddressSpaces());
		});
		map.put("usersMap", users);

		areaMap.set("map", map);
		return areaMap;
	}
}
