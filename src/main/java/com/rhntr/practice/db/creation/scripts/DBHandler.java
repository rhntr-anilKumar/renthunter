package com.rhntr.practice.db.creation.scripts;

import java.io.NotActiveException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBHandler {

	private static Connection conn = null;
	
	public static Connection getConnection()	{
        String url = "jdbc:oracle:thin:@localhost:1521:xe"; 
        if(conn == null)	{
            synchronized (DBHandler.class) {
        		try {
        			conn = DriverManager.getConnection(url,"Maddy","root");
        		} catch (SQLException e) {
        			e.printStackTrace();
        		}
    		}
        }
        return conn;
	}
	
	public static int getNextId(String tableName)	throws Exception {
		
		if(conn == null){
			throw new NotActiveException("Connection is not active");
		}
		
		int nextId = -1;
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT MAX(id) FROM " + tableName);
		if(rs.next())	{
			nextId = rs.getInt(1);
		}
		return nextId + 1;
	}

	
	public static List<Integer> getPrimaryKeyIds(String tableName, String whereCondition) throws Exception  {

		if(conn == null){
			throw new NotActiveException("Connection is not active");
		}
		
		List<Integer> ids = new ArrayList<Integer>();
		Statement st = conn.createStatement();
		String query = "SELECT id FROM " + tableName;
		if(whereCondition != null)	{
			query += " WHERE " + whereCondition;
		}
		ResultSet rs = st.executeQuery(query);
		while(rs.next())	{
			ids.add(rs.getInt(1));
		}
		return ids;
	}

	public static List<Integer> getPrimaryKeyIds(String tableName) throws Exception  {
		return getPrimaryKeyIds(tableName, null);
	}

	
	public static void closeConnection()	throws Exception 	{
		synchronized (DBHandler.class) {
			if(conn != null)	{
				conn.close();
			}
		}
	}
}
