package com.rhntr.practice.db.creation.scripts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONObject;

public class GoogleAddressInserts {
	public static void main (String[] args) {
		
		try {
			JSONObject addressObject = getJSONObject("src/main/java/com/rhntr/practice"
					+ "/db/creation/scripts/googled_address.json");

			 try {
		            String url = "jdbc:oracle:thin:@localhost:1521:xe"; 
		            Connection conn = DriverManager.getConnection(url,"Maddy","root");
		            Statement st = conn.createStatement();

		            int length = addressObject.getJSONArray("object").length();
		            for(int i = 0; i < length; i++ )	{
		            	JSONObject add = (JSONObject) addressObject.getJSONArray("object").get(i);
		            	System.out.println(add);
		            	String query = "INSERT INTO GOOGLED_ADDRESS "
				            	+ "(ID, LATITUDE, LONGITUDE, PLACE_ID, FORMATTED_ADDRESS, "
				            	+ "STREET_NO, STREET, ROUTE, SUBLOCALITY_LEVEL_2, SUBLOCALITY_LEVEL_1, "
				            	+ "LOCALITY, ADMINISTRATIVE_AREA_LEVEL_2, ADMINISTRATIVE_AREA_LEVEL_1, "
				            	+ "COUNTRY, POSTAL_CODE) "
				            	
				            	+ "VALUES ("+ i + ", " + add.get("latitude") + ", " 
				            		+ add.get("longitude") + ", '" + add.get("placeId") + "', '" 
				            		+ add.get("formattedAddress") + "', '" + add.get("streetNumber") + "', '" 
				            		+ add.get("street") + "', '" + add.get("route") + "', '" + add.get("sublocalityLevel2") + "', '" 
				            		+ add.get("sublocalityLevel1") + "', '" + add.get("locality") + "', '" + add.get("administrativeAreaLevel2") 
				            		+ "', '" + add.get("administrativeAreaLevel1") + "', '" + add.get("country") + "', '" + add.get("postalCode") + "')";
		            	System.out.println(add.get("postalCode"));
		            	System.out.println(query);
		            	
		            	try	{
				            st.executeUpdate(query);
				            i++;
		            	} catch(SQLException ex)	{
		            		System.out.println(ex.getLocalizedMessage());
		            	}
		            }
		            
		            //{"id":1,"latitude":12.843063,"longitude":77.65648,"placeId":"ChIJZ_hTxoRsrjsR-So1cnwXt6I","formattedAddress":"Electronics City Phase 1, Electronic City, Bengaluru, Karnataka, India","streetNumber":null,"street":null,"route":null,"sublocalityLevel2":"Electronics City Phase 1","sublocalityLevel1":"Electronic City","locality":"Bengaluru","administrativeAreaLevel2":"Bangalore Urban","administrativeAreaLevel1":"Karnataka","country":"India","postalCode":"560100"}
		            
		            conn.close();
		        } catch (Exception e) {
		            System.err.println("Got an exception! ");
		            System.err.println(e.getMessage());
		        } 

		} catch (Exception e)	{
			System.out.println("Error : " + e.getLocalizedMessage());
		}
	}

	public static JSONObject getJSONObject(String filename) throws Exception	{
	    String result = "";
	    try {
	        BufferedReader br = new BufferedReader(new FileReader(filename));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        result = sb.toString();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return new JSONObject(result);
	}
}