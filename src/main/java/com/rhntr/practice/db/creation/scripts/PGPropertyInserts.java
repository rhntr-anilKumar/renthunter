package com.rhntr.practice.db.creation.scripts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class PGPropertyInserts {

	private static Random rand = new Random();
	
	public static enum PGSharingSpace {
		SINGLE_SHARING, DOUBLE_SHARING, THREE_SHARING, FOUR_SHARING, OPEN_SHARING, ANY_SHARING
	}


	public static void main1(String[] args) {
		IntStream.range(0, 4).forEach(System.out::println);
	}
	
	public static void main(String[] args) throws Exception {

		Connection con = DBHandler.getConnection();

		String query = new String("INSERT INTO RH_PG_SPACE_PROPERTIES "
				+ "(ID, AVAILABLE, MONTHLY_RENT, SHARING, PAYINGGUEST_ID) "
				+ "VALUES (?, ?, ?, ?, ?)");

		PreparedStatement ps = con.prepareStatement(query);

		List<Integer> pgIds = DBHandler.getPrimaryKeyIds("RH_PG_SPACE");

		try {
			pgIds.stream().filter(f -> f > 881).forEach(pgId -> {
				IntStream.range(0, 5).forEach(sharing -> {
					int amount = 0;
					switch (sharing) {
						case 0:	amount = getRoundedAmount(6000, 8000);	break;
						case 1:	amount = getRoundedAmount(5500, 7000);	break;
						case 2:	amount = getRoundedAmount(5000, 6000);	break;
						case 3:	amount = getRoundedAmount(3500, 5500);	break;
						case 4:	amount = getRoundedAmount(3000, 4500);	break;
					}

					try {
						int nextId = DBHandler.getNextId("RH_PG_SPACE_PROPERTIES");
						ps.setInt(1, nextId);
						ps.setBoolean(2, true);
						ps.setInt(3, amount);
						ps.setInt(4, sharing);
						ps.setInt(5, pgId);

						if(ps.execute())	{
							System.out.println(nextId +  " -> Failed at " + pgId);
						} else {
							System.out.println(nextId +  " -> Success for " + pgId);
						}
					} catch (Exception e) {
						try{
							System.out.println("closing connection....");
							ps.close();
							DBHandler.closeConnection();
						} catch(Exception ex)	{	}
						e.printStackTrace();
						System.exit(0);
					}
				});
			});
		} catch(Exception ex)	{
			
		} finally	{
			System.out.println("closing connection....");
			ps.close();
			DBHandler.closeConnection();
		}
	}


	private static int getInt(int from, int to) {
		return rand.ints(from, to).limit(1).findFirst().getAsInt();
	}


	private static int getRoundedAmount(int from, int to) {
		int num = rand.ints(from, to).limit(1).findFirst().getAsInt();
		int rem = num % 500;
		return num - rem;
	}
}