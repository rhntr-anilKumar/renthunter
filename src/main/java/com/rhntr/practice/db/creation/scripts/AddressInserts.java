package com.rhntr.practice.db.creation.scripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Random;

public class AddressInserts {

	public static void main (String[] args) throws Exception	{

		Random rand = new Random();
		String query = "INSERT INTO RH_ADDRESS_SPACE (ID, PROPERTY_TYPE, GOOGLEDADDRESS_ID, USER_ID) VALUES (?, ?, ?, ?)";

		Connection conn = DBHandler.getConnection();
		PreparedStatement ps = conn.prepareStatement(query);

		List<Integer> addressIds = DBHandler.getPrimaryKeyIds("GOOGLED_ADDRESS");
		int addIdsLength = addressIds.size();

		List<Integer> userIds = DBHandler.getPrimaryKeyIds("RH_USERS");
		int userIdsLength = userIds.size();

		for(int i = 0; i < 300; i++)	{

			int nextId = DBHandler.getNextId("RH_ADDRESS_SPACE");
			ps.setInt(1, nextId);
			ps.setString(2, "PG");
			ps.setInt(3, addressIds.get(rand.nextInt(addIdsLength)));
			ps.setInt(4, userIds.get(rand.nextInt(userIdsLength)));
			try	{
				ps.executeUpdate();
				System.out.println(nextId + " ---> success.");
			} catch(SQLException ex)	{
				System.out.println(nextId  + " :: " + ex.getLocalizedMessage());
			}
		}

		ps.close();
		DBHandler.closeConnection();
	}
}