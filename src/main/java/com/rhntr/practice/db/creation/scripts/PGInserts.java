package com.rhntr.practice.db.creation.scripts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PGInserts {

	private static Random rand = new Random();
	
	public static enum PGType {
		BOYS, GIRLS
	}


	public static void main(String[] args) throws Exception {

		PGType[] pgTypes = {PGType.BOYS, PGType.GIRLS};
		
		Connection con = DBHandler.getConnection();

		String query = new String("INSERT INTO RH_PG_SPACE (ID, ADVANCE, AVAILBLE_ROOMS, FOOD_TYPE,  "
				+ "PG_NAME, SECURITY_GUARD, TOTAL_ROOMS, PG_TYPE, CC_TV, WIFI, ADDRESSSPACE_ID) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement ps = con.prepareStatement(query);

		List<Integer> addressIds = DBHandler.getPrimaryKeyIds("RH_ADDRESS_SPACE", "property_type = 'PG'");
		int addIdsLength = addressIds.size();

		for(int i = 0 ; i < 500; i++)	{
			int nextId = DBHandler.getNextId("RH_PG_SPACE");
			int totalRooms = getInt(10, 61);
			ps.setInt(1, nextId);
			ps.setInt(2, getRoundedAmount(3100, 6501));
			ps.setInt(3, rand.nextInt(totalRooms));
			ps.setInt(4, rand.nextInt(2));
			ps.setString(5, "PG " + nextId);
			ps.setBoolean(6, rand.nextBoolean());
			ps.setInt(7, totalRooms);
			ps.setString(8, pgTypes[rand.nextInt(2)].toString());
			ps.setBoolean(9, rand.nextBoolean());
			ps.setBoolean(10, rand.nextBoolean());
			ps.setInt(11, addressIds.get(rand.nextInt(addIdsLength)));
			
			if(ps.execute())	{
				System.out.println(nextId +  " -> Failed");
			} else {
				System.out.println(nextId +  " -> Success");
			}
		}
		ps.close();
		DBHandler.closeConnection();
	}


	private static int getInt(int from, int to) {
		return rand.ints(from, to).limit(1).findFirst().getAsInt();
	}


	private static int getRoundedAmount(int from, int to) {
		int num = rand.ints(from, to).limit(1).findFirst().getAsInt();
		int rem = num % 500;
		return num - rem;
	}
}


/*		ID
ADVANCE
AVAILBLE_ROOMS
FOOD_TYPE
HOT_WATER
PG_NAME
SECURITY_GUARD
TOTAL_ROOMS
PG_TYPE
CC_TV
WIFI
ADDRESSSPACE_ID
FURNISHED		
*/