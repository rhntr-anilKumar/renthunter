package com.rhntr.core.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rhntr.core.model.entities.Shops;

public interface ShopRepository extends JpaRepository<Shops, Long> {

}
