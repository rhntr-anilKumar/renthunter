package com.rhntr.core.model.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.rhntr.application.dto.HomeDTO;
import com.rhntr.core.model.entities.Home;

public interface HomeRepository extends JpaRepository<Home, Long> {

	List<Home> findByAddressSpaceIdIn(List<Long> addressIds);

	@Query("SELECT home FROM Home home WHERE home.addressSpace.propertyType = 'HOME' AND home.addressSpace.googledAddress.id IN (:googleAddressIds)")
	List<Home> findByGoogledAddressIds(@Param("googleAddressIds") Collection<Long> googleAddressIds);
	
	@Query("SELECT new com.rhntr.application.dto.HomeDTO(home.id, hsp.rent, hsp.deposit, hsp.bhkLength, home.buildUpArea, gAddSpc.formattedAddress) "
			+ "FROM Home home LEFT JOIN home.spaceProperties hsp "
			+ "LEFT JOIN home.addressSpace addSpc "
			+ "LEFT JOIN addSpc.googledAddress gAddSpc "
			+ "ORDER BY home.postedOn")
	List<HomeDTO> recentPostedProperty();
}