package com.rhntr.core.model.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.rhntr.core.model.entities.PayingGuest;

public interface PgRepository extends JpaRepository<PayingGuest, Long>{

	List<PayingGuest> findByAddressSpaceIdIn(List<Long> addressIds);

	@Query("SELECT PG FROM PayingGuest PG WHERE PG.addressSpace.propertyType = 'PG' AND PG.addressSpace.googledAddress.id IN (:googleAddressIds)")
	List<PayingGuest> findByGoogledAddressIds(@Param("googleAddressIds") Collection<Long> googleAddressIds);
}
