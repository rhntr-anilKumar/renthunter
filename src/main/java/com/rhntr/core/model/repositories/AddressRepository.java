package com.rhntr.core.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.rhntr.core.model.entities.AddressSpace;

public interface AddressRepository extends JpaRepository<AddressSpace, Long> {

	
	@Query("FROM AddressSpace WHERE user.id = :userId")
	public List<AddressSpace> getByUserId(@Param("userId") Long userId) throws Exception;
}
