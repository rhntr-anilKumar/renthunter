package com.rhntr.core.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rhntr.core.model.entities.GoogledAddress;

public interface GAddressRepository extends JpaRepository<GoogledAddress, Long> {

	public GoogledAddress getByPlaceIdAndFormattedAddress(String placeId, String formattedAddress);
}
