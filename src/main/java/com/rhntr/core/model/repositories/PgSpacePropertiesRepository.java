package com.rhntr.core.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rhntr.core.model.entities.PGSpaceProperties;

public interface PgSpacePropertiesRepository extends JpaRepository<PGSpaceProperties, Long> {

}
