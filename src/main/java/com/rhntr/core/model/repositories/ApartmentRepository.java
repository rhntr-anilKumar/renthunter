package com.rhntr.core.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.rhntr.application.dto.ApartmentDTO;
import com.rhntr.core.model.entities.Apartment;

public interface ApartmentRepository extends JpaRepository<Apartment, Long> {
	
	List<Apartment> findByAddressSpaceIdIn(List<Long> addressIds);

	@Query("SELECT new com.rhntr.application.dto.ApartmentDTO(apartment.id, apartment.rent, apartment.deposit, "
			+ "apartment.bhkLength, apartment.buildUpArea, gAddSpc.formattedAddress) "
			+ "FROM Apartment apartment JOIN apartment.addressSpace addSpc "
			+ "JOIN addSpc.googledAddress gAddSpc "
			+ "ORDER BY apartment.postedOn")
	List<ApartmentDTO> recentPostedProperty();
}
