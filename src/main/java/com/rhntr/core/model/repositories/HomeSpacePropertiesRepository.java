package com.rhntr.core.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rhntr.core.model.entities.HomeSpaceProperties;

public interface HomeSpacePropertiesRepository extends JpaRepository<HomeSpaceProperties, Long> {

}
