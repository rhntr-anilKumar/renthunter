package com.rhntr.core.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rhntr.core.model.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

	boolean existsByEmailIdOrPhoneNumber(String emailId, String phoneNumber);

	User findByEmailIdOrPhoneNumber(String emailId, String phoneNumber);
}
