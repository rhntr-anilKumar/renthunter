package com.rhntr.core.model.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.rhntr.core.enums.ParkingSpace;
import com.rhntr.core.enums.TenantsType;

/**
 * <b>Home		</b> <br/> <br/>
 * 
 * Home is record of all registered rooms's for their online hosting of rooms availability
 * and attracting users for their home with optional integrated furnished specialties.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 25-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link HomeSpaceProperties}
 * 
 */

@Entity
@Table(name = "RH_HOME_SPACE")
public class Home	extends	AbstractEntity	{

	private static final long serialVersionUID = 1L;

	private String name;
	private Date constructedOn;
	private Date postedOn;
	private TenantsType tenant;
	private Integer buildUpArea;

	private Boolean nonVegAllowed;
	private Boolean petsAllowed;
	private ParkingSpace parking;


	private Long rent;
	private Long deposit;
	private Boolean negotiable;
	private Boolean depositRefundable;	
	private Integer maintainanceCharge;
	private Integer agreemantDuriation;	
	private short leavingIntimationBefore;
	
	private Integer availableRooms;
	private Integer totalRooms;
	
	private HomeSpaceProperties spaceProperties;

	private AddressSpace addressSpace;
	
	private List<HomePictures> homePictures;

	/**
	 * default constructor
	 */
	public Home() {
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the nonVegAllowed
	 */
	@Type(type = "true_false")
	@Column(name = "non_veg")
	public Boolean isNonVegAllowed() {
		return nonVegAllowed;
	}

	/**
	 * @param nonVegAllowed the nonVegAllowed to set
	 */
	public void setNonVegAllowed(Boolean nonVegAllowed) {
		this.nonVegAllowed = nonVegAllowed;
	}

	/**
	 * @return the availableRooms
	 */
	@Column(name = "rooms_available")
	public Integer getAvailableRooms() {
		return availableRooms;
	}

	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(Integer availableRooms) {
		this.availableRooms = availableRooms;
	}

	/**
	 * @return the totalRooms
	 */
	@Column(name = "total_rooms")
	public Integer getTotalRooms() {
		return totalRooms;
	}

	/**
	 * @param totalRooms the totalRooms to set
	 */
	public void setTotalRooms(Integer totalRooms) {
		this.totalRooms = totalRooms;
	}

	/**
	 * @return the petsAllowed
	 */
	@Type(type = "true_false")
	@Column(name = "pets_allowed")
	public Boolean isPetsAllowed() {
		return petsAllowed;
	}

	/**
	 * @param petsAllowed the petsAllowed to set
	 */
	public void setPetsAllowed(Boolean petsAllowed) {
		this.petsAllowed = petsAllowed;
	}

	/**
	 * @return parking
	 */
	@Enumerated(value = EnumType.STRING)
	@Column(name = "parking")
	public ParkingSpace getParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(ParkingSpace parking) {
		this.parking = parking;
	}
	
	/**
	 * @return the negotiable
	 */
	@Type(type = "true_false")
	@Column(name = "negotiable")
	public Boolean isNegotiable() {
		return negotiable;
	}

	/**
	 * @param isNegotiable the negotiable to set
	 */
	public void setNegotiable(Boolean negotiable) {
		this.negotiable = negotiable;
	}

	/**
	 * @return the spaceProperties
	 */
	@OneToOne(fetch = FetchType.LAZY,
	    cascade =  CascadeType.ALL,
	    mappedBy = "home"
	)
	@JsonManagedReference
	public HomeSpaceProperties getSpaceProperties() {
		return spaceProperties;
	}

	/**
	 * @param spaceProperties the spaceProperties to set
	 */
	public void setSpaceProperties(HomeSpaceProperties spaceProperties) {
		this.spaceProperties = spaceProperties;
	}

	/**
	 * @return the addressSpace
	 */
	@OneToOne (cascade = CascadeType.ALL)
	public AddressSpace getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(AddressSpace addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the constructedOn
	 */
	@Column(name = "doc")
	public Date getConstructedOn() {
		return constructedOn;
	}

	/**
	 * @param constructedOn the constructedOn to set
	 */
	public void setConstructedOn(Date constructedOn) {
		this.constructedOn = constructedOn;
	}

	/**
	 * @return the postedOn
	 */
	@Column(name = "posted_on")
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * @param postedOn the postedOn to set
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	/**
	 * @return the tenant
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "tenant_type")
	public TenantsType getTenant() {
		return tenant;
	}

	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(TenantsType tenant) {
		this.tenant = tenant;
	}

	/**
	 * @return the depositRefundable
	 */
	@Type(type = "true_false")
	@Column(name = "deposite_refundable")
	public Boolean getDepositRefundable() {
		return depositRefundable;
	}

	/**
	 * @param depositRefundable the depositRefundable to set
	 */
	public void setDepositRefundable(Boolean depositRefundable) {
		this.depositRefundable = depositRefundable;
	}

	/**
	 * @return the maintainanceCharge
	 */
	@Column(name = "maintainance_charge")
	public Integer getMaintainanceCharge() {
		return maintainanceCharge;
	}

	/**
	 * @param maintainanceCharge the maintainanceCharge to set
	 */
	public void setMaintainanceCharge(Integer maintainanceCharge) {
		this.maintainanceCharge = maintainanceCharge;
	}

	/**
	 * @return the agreemantDuriation
	 */
	@Column(name = "agreement_duration")
	public Integer getAgreemantDuriation() {
		return agreemantDuriation;
	}

	/**
	 * @param agreemantDuriation the agreemantDuriation to set
	 */
	public void setAgreemantDuriation(Integer agreemantDuriation) {
		this.agreemantDuriation = agreemantDuriation;
	}

	/**
	 * @return the leavingIntimation
	 */
	@Column(name = "leaving_intimation_before")
	public short getLeavingIntimationBefore() {
		return leavingIntimationBefore;
	}

	/**
	 * @param leavingIntimation the leavingIntimation to set
	 */
	public void setLeavingIntimationBefore(short leavingIntimation) {
		this.leavingIntimationBefore = leavingIntimation;
	}

	/**
	 * @return the buildUpArea
	 */
	@Column(name = "buildup_area_sqfeet")
	public Integer getBuildUpArea() {
		return buildUpArea;
	}

	/**
	 * @param buildUpArea the buildUpArea to set
	 */
	public void setBuildUpArea(Integer buildUpArea) {
		this.buildUpArea = buildUpArea;
	}

	/**
	 * @return the rent
	 */
	@Column(name = "rent")
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the deposit
	 */
	@Column(name = "deposit")
	public Long getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(Long deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the homePictures
	 */
	@OneToMany(
		cascade = CascadeType.ALL, 
		fetch = FetchType.LAZY, 
		mappedBy = "home"
	)
	@JsonManagedReference
	public List<HomePictures> getHomePictures() {
		return homePictures;
	}

	/**
	 * @param homePictures the homePictures to set
	 */
	public void setHomePictures(List<HomePictures> homePictures) {
		this.homePictures = homePictures;
	}
}