package com.rhntr.core.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.rhntr.core.enums.PropertyType;

/**
 * <b>RentHunterApplication		</b> <br/> <br/>
 * 
 * Address space is list of all hosted owners residential addresses locating their homes and sites.
 * To store all owner user addresses and site information.
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 23-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		UJ Madhu
 * @see			{@link User}
 */

@Entity
@Table(name = "RH_ADDRESS_SPACE")
public class AddressSpace	extends	AbstractEntity	{

	private static final long serialVersionUID = 1L;

    private User user;

	private PropertyType propertyType;

	private GoogledAddress googledAddress;
	
	/**
	 * default constructor
	 */
	public AddressSpace() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the spaceType
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "property_type")
	public PropertyType getPropertyType() {
		return propertyType;
	}

	/**
	 * @param spaceType the spaceType to set
	 * 
	 */
	public void setPropertyType(PropertyType spaceType) {
		this.propertyType = spaceType;
	}

	/**
	 * @return the user
	 */
	@JsonBackReference
	@ManyToOne
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * @return
	 */
	@Transient
	public Long getUserId()	{
		if(this.user != null)	{
			return this.user.getId();
		}
		return null;
	}

	/**
	 * @return the googledAddress
	 */
	@ManyToOne
	public GoogledAddress getGoogledAddress() {
		return googledAddress;
	}

	/**
	 * @param googledAddress the googledAddress to set
	 */
	public void setGoogledAddress(GoogledAddress googledAddress) {
		this.googledAddress = googledAddress;
	}
}