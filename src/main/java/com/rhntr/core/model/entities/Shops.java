package com.rhntr.core.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RH_SHOPS")
public class Shops	extends	AbstractEntity	{

	private static final long serialVersionUID = 1L;

	private Long squareFeet;
	
	private Long width;
	
	private Long length;
	
	private Long deposit;
	
	private Long rent;
	
	private AddressSpace addressSpace;
	

	/**
	 * Still under requirement forming...
	 */
	
	/**
	 * default constructor
	 * @throws Exception 
	 */
	public Shops(){
		
	}


	/**
	 * @return the squareFeet
	 */
	public Long getSquareFeet() {
		return squareFeet;
	}

	/**
	 * @param squareFeet the squareFeet to set
	 */
	public void setSquareFeet(Long squareFeet) {
		this.squareFeet = squareFeet;
	}

	/**
	 * @return the deposite
	 */
	public Long getDeposit() {
		return deposit;
	}

	/**
	 * @param deposite the deposite to set
	 */
	public void setDeposit(Long deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the rent
	 */
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the addressSpace
	 */
	@OneToOne(cascade = CascadeType.ALL)
	//@PrimaryKeyJoinColumn
	public AddressSpace getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(AddressSpace addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the width
	 */
	public Long getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(Long width) {
		this.width = width;
	}

	/**
	 * @return the length
	 */
	public Long getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(Long length) {
		this.length = length;
	}
}