package com.rhntr.core.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "RH_APARTMENT_PICTURES")
public class ApartmentPictures extends FilesList	{
	
	private Apartment apartment;

	
	/**
	 * default constructor
	 */
	public ApartmentPictures() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the apartment
	 */
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	public Apartment getApartment() {
		return apartment;
	}

	/**
	 * @param apartment the apartment to set
	 */
	public void setApartment(Apartment apartment) {
		this.apartment = apartment;
	}
}