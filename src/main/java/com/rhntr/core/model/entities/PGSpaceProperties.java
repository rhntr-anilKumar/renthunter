package com.rhntr.core.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.rhntr.core.enums.PGSharingType;

/**
 * <b>PGSpaceProperties		</b> <br/> <br/>
 * 
 * PGSpaceProperties is additional common properties for Paying Guest registered users details.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 01-Feb-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link PayingGuest}
 * 
 */

@Entity
@Table(name = "RH_PG_SPACE_PROPERTIES")
public class PGSpaceProperties	extends	AbstractEntity	{
	
	private static final long serialVersionUID = 1L;

	private PGSharingType sharingType;

	private Long monthlyRent;

	private Integer totalRooms;

	private Integer availableRooms;
	
	private PayingGuest payingGuest;


	/**
	 * default constructor
	 */
	public PGSpaceProperties() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the sharingSpace
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "sharing")
	public PGSharingType getSharingType() {
		return sharingType;
	}
	
	@Transient
	public int getSharing()	{
		return sharingType.ordinal() + 1;
	}

	/**
	 * @param sharingSpace the sharingSpace to set
	 */
	public void setSharingType(PGSharingType sharingSpace) {
		this.sharingType = sharingSpace;
	}

	/**
	 * @return the monthlyRent
	 */
	@Column(name = "monthly_rent")
	public Long getMonthlyRent() {
		return monthlyRent;
	}

	/**
	 * @param monthlyRent the monthlyRent to set
	 */
	public void setMonthlyRent(Long monthlyRent) {
		this.monthlyRent = monthlyRent;
	}

	/**
	 * @return the totalSpace
	 */
	@Column(name = "total_rooms")
	public Integer getTotalRooms() {
		return totalRooms;
	}

	/**
	 * @param totalSpace the totalSpace to set
	 */
	public void setTotalRooms(Integer totalRooms) {
		this.totalRooms = totalRooms;
	}

	/**
	 * @return the availableRooms
	 */
	@Column(name = "available_rooms")
	public Integer getAvailableRooms() {
		return availableRooms;
	}

	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(Integer availableRooms) {
		this.availableRooms = availableRooms;
	}

	/**
	 * @return the payingGuest
	 */
	@JsonBackReference
	@ManyToOne
	public PayingGuest getPayingGuest() {
		return payingGuest;
	}

	/**
	 * @param payingGuest the payingGuest to set
	 */
	public void setPayingGuest(PayingGuest payingGuest) {
		this.payingGuest = payingGuest;
	}
}