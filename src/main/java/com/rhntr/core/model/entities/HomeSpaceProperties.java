package com.rhntr.core.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.rhntr.core.enums.Availability;
import com.rhntr.core.enums.Furnished;
import com.rhntr.core.enums.RentHomeSpace;

/**
 * <b>RentSpaceProperties		</b> <br/> <br/>
 * 
 * RentSpaceProperties is additional common properties for room/rent registered users details.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 01-Feb-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link Home}
 * 
 */

@Entity
@Table(name = "RH_HOME_SPACE_PROPERTIES")
public class HomeSpaceProperties 	extends	AbstractEntity	{
	
	private static final long serialVersionUID = 1L;

	private short floor;

	private Furnished furnished;

	private RentHomeSpace bhkLength;
	
	private Long deposit;

	private Long rent;
	
	private Availability availability;
	
	private Home home;
	
	/**
	 * default constructor
	 */
	public HomeSpaceProperties() {
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * @return the floor
	 */
	@Column(name = "floor")
	public short getFloor()	{
		return floor;
	}

	/**
	 * @param floar the floor to set
	 */
	public void setFloor(short floor) {
		this.floor = floor;
	}

	/**
	 * @return the furnished
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "furnished")
	public Furnished getFurnished() {
		return furnished;
	}

	/**
	 * @param furnished the furnished to set
	 */
	public void setFurnished(Furnished furnished) {
		this.furnished = furnished;
	}

	/**
	 * @return the bhkLength
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "bhk_length")
	public RentHomeSpace getBhkLength() {
		return bhkLength;
	}

	/**
	 * @param bhkLength the bhkLength to set
	 */
	public void setBhkLength(RentHomeSpace bhkLength) {
		this.bhkLength = bhkLength;
	}

	/**
	 * @return the deposit
	 */
	@Column(name = "deposit")
	public Long getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(Long deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the rent
	 */
	@Column(name = "monthly_rent")
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the availability
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "availability")
	public Availability getAvailability() {
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	/**
	 * @return the home
	 */
	@JsonBackReference
	@OneToOne(fetch = FetchType.LAZY)
	public Home getHome() {
		return home;
	}

	/**
	 * @param home the home to set
	 */
	public void setHome(Home home) {
		this.home = home;
	}
}