package com.rhntr.core.model.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public class AbstractEntity implements java.io.Serializable, Comparable<AbstractEntity>	{

	private static final long serialVersionUID = 1L;

	protected Long id;
	
	/**
	 * @return the id
	 */
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 *	default implementation of {@link Object#equals(Object)} method for entities.
	 * 
	 * 	@param obj
	 * 	@return boolean
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null || this.id == null)	return false;
		
		if(!(obj instanceof AbstractEntity))	return false;
		
		AbstractEntity entity = (AbstractEntity) obj;
		
		return this == obj || this.id.equals(entity.id);
	}
	
	/**
	 *	default implementation of {@link Object#hashCode()} based on id of entities.
	 * 
	 *  @return integer
	 */
	@Override
	public int hashCode() {
		int res = 17;
		return 31 * res + this.id.hashCode();
	}

	/**
	 * 	default implementation of {@link Comparable#compareTo(Object)} for entity level comparison.
	 * 
	 * 	@param enity
	 * 	@return integer
	 */
	@Override
	public int compareTo(AbstractEntity enity) {		
		return this.id.compareTo(enity.id);
	}
}