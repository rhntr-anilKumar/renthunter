package com.rhntr.core.model.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class FilesList implements Comparable<FilesList>{

	private String fileName;
	
	private String originalFileName;
	
	private String fileDownloadUri;
	
	private String fileType;
	
	private Integer size;


	/**
	 * Constructor
	 */
	public FilesList() {
		
	}

	/**
	 * @return the fileName
	 */
	@Id
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the originalFileName
	 */
	@Column(name = "original_name")
	public String getOriginalFileName() {
		return originalFileName;
	}

	/**
	 * @param originalFileName the originalFileName to set
	 */
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	/**
	 * @return the fileDownloadUri
	 */
	@Transient
	public String getFileDownloadUri() {
		return fileDownloadUri;
	}

	/**
	 * @param fileDownloadUri the fileDownloadUri to set
	 */
	public void setFileDownloadUri(String fileDownloadUri) {
		this.fileDownloadUri = fileDownloadUri;
	}

	/**
	 * @return the fileType
	 */
	@Column(name = "file_type")
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	@Column(name = "size")
	public void setSize(Integer size) {
		this.size = size;
	}
	
	
	/**
	 * 	default implementation of {@link Object#equals(Object)} method for files items.
	 * 
	 * 	@param obj
	 *	@return boolean
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null || this.fileName == null)	return false;
		
		if(!(obj instanceof FilesList))	return false;
		
		FilesList list = (FilesList) obj;
		
		return this == obj || this.fileName.equals(list.fileName);
	}

	/**
	 *	default implementation of {@link Object#hashCode()} based on file name of file list.
	 * 
	 *  @return integer
	 */
	@Override
	public int hashCode() {
		int res = 17;
		return 31 * res + this.fileName.hashCode();
	}

	/**
	 * 	default implementation of {@link Comparable#compareTo(Object)} for file item level comparison.
	 * 
	 * 	@param item
	 * 	@return integer
	 */
	@Override
	public int compareTo(FilesList item) {
		return this.fileName.compareTo(item.fileName);
	}
}