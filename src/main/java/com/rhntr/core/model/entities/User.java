package com.rhntr.core.model.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <b>User			</b> <br/> <br/>
 * 
 * Users are the list of both normal users and owner of the sites/homes/apartments
 * Owner users have special categories those can have options for hosting 
 * their sites, homes, apartments and any type of lands.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 23-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		UJ Madhu
 * @see			{@link AddressSpace}
 * 
 */

@Entity
@Table(name = "RH_USERS")
public class User	extends	AbstractEntity	{
	
	private static final long serialVersionUID = 1L;

	private String passcode;

	private String firstName;
	
	private String lastName;
	
	private Date dateOfBirth;
	
	private String emailId;
	
	private String phoneNumber;
	
	private Date registrationOfDate;
	
	private List<AddressSpace> addressSpaces;

	/**
	 * default constructor
	 */
	public User() {
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * @return the passcode
	 */
	@JsonIgnore
	@Column(name = "passcode", length = 100)
	public String getPasscode() {
		return passcode;
	}

	/**
	 * @param passcode the passcode to set
	 */
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	/**
	 * @return the firstName
	 */
	@Column(name = "firstname")
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	@Column(name = "lastname")
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	@Column(name = "dob")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the emailId
	 */
	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the phoneNumber
	 */
	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the registrationOfDate
	 */
	@Column(name = "reg_date")
	public Date getRegistrationOfDate() {
		return registrationOfDate;
	}

	/**
	 * @param registrationOfDate the registrationOfDate to set
	 */
	public void setRegistrationOfDate(Date registrationOfDate) {
		this.registrationOfDate = registrationOfDate;
	}

	/**
	 * @return the addressSpaces
	 */
	@OneToMany(
		cascade = CascadeType.ALL, 
		fetch = FetchType.LAZY, 
		mappedBy = "user"
	)
	@JsonIgnore
	public List<AddressSpace> getAddressSpaces() {
		return addressSpaces;
	}
	

	/**
	 * @param addressSpaces the addressSpaces to set
	 */
	public void setAddressSpaces(List<AddressSpace> addressSpaces) {
		this.addressSpaces = addressSpaces;
	}

	@Transient
	public List<Long> getAddressSpacesId() {
		if(addressSpaces == null)	return new ArrayList<Long>();
		return addressSpaces.stream().map(address -> address.getId())
		.collect(Collectors.toList());
	}
}