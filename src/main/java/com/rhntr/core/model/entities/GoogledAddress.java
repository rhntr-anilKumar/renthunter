package com.rhntr.core.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <b>GoogleAddress	</b> <br/> <br/>
 * 
 * GoogleAddress is reference format for storing user address details.
 * It includes general details for locating user property.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 22-Sep-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AddressSpace}
 * 
 */

@Entity
@Table(name = "GOOGLED_ADDRESS")
public class GoogledAddress extends	AbstractEntity{

	private static final long serialVersionUID = 1L;

	private Float latitude;

	private Float longitude;

    private String placeId;

    private String formattedAddress;

    private String streetNumber;

    private String street;

    private String route;

    private String sublocalityLevel2;

    private String sublocalityLevel1;

    private String locality;

    private String administrativeAreaLevel2;

    private String administrativeAreaLevel1;

    private String country;

    private String postalCode;

    private List<AddressSpace> addressSpace;
    

    /**
	 * @return the latitude
	 */
	@Column(name = "latitude")
	public Float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	@Column(name = "longitude")
	public Float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the placeId
	 */
	@Column(name = "place_id", unique = true)
	public String getPlaceId() {
		return placeId;
	}

	/**
	 * @param placeId the placeId to set
	 */
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	/**
	 * @return the fullAddress
	 */
	@Column(name = "formatted_address", unique = true)
	public String getFormattedAddress() {
		return formattedAddress;
	}

	/**
	 * @param fullAddress the fullAddress to set
	 */
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}

	/**
	 * @return the streetNumber
	 */
	@Column(name = "street_no")
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @param streetNumber the streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * @return the street
	 */
	@Column(name = "street")
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the route
	 */
	@Column(name = "route")
	public String getRoute() {
		return route;
	}

	/**
	 * @param route the route to set
	 */
	public void setRoute(String route) {
		this.route = route;
	}

	/**
	 * @return the sublocalityLevel2
	 */
	@Column(name = "sublocality_level_2")
	public String getSublocalityLevel2() {
		return sublocalityLevel2;
	}

	/**
	 * @param sublocalityLevel2 the sublocalityLevel2 to set
	 */
	public void setSublocalityLevel2(String sublocalityLevel2) {
		this.sublocalityLevel2 = sublocalityLevel2;
	}

	/**
	 * @return the sublocalityLevel1
	 */
	@Column(name = "sublocality_level_1")
	public String getSublocalityLevel1() {
		return sublocalityLevel1;
	}

	/**
	 * @param sublocalityLevel1 the sublocalityLevel1 to set
	 */
	public void setSublocalityLevel1(String sublocalityLevel1) {
		this.sublocalityLevel1 = sublocalityLevel1;
	}

	/**
	 * @return the locality
	 */
	@Column(name = "locality")
	public String getLocality() {
		return locality;
	}

	/**
	 * @param locality the locality to set
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}

	/**
	 * @return the administrativeAreaLevel2
	 */
	@Column(name = "administrative_area_level_2")
	public String getAdministrativeAreaLevel2() {
		return administrativeAreaLevel2;
	}

	/**
	 * @param administrativeAreaLevel2 the administrativeAreaLevel2 to set
	 */
	public void setAdministrativeAreaLevel2(String administrativeAreaLevel2) {
		this.administrativeAreaLevel2 = administrativeAreaLevel2;
	}

	/**
	 * @return the administrativeAreaLevel1
	 */
	@Column(name = "administrative_area_level_1")
	public String getAdministrativeAreaLevel1() {
		return administrativeAreaLevel1;
	}

	/**
	 * @param administrativeAreaLevel1 the administrativeAreaLevel1 to set
	 */
	public void setAdministrativeAreaLevel1(String administrativeAreaLevel1) {
		this.administrativeAreaLevel1 = administrativeAreaLevel1;
	}

	/**
	 * @return the country
	 */
	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the postalCode
	 */
	@Column(name = "postal_code")
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the addressSpace
	 */
	@OneToMany(
			cascade = CascadeType.ALL, 
			fetch = FetchType.LAZY, 
			mappedBy = "googledAddress"
		)
	@JsonIgnore
	public List<AddressSpace> getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(List<AddressSpace> addressSpace) {
		this.addressSpace = addressSpace;
	}	
}
