package com.rhntr.core.model.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.rhntr.core.enums.Furnished;
import com.rhntr.core.enums.ParkingSpace;
import com.rhntr.core.enums.RentHomeSpace;
import com.rhntr.core.enums.TenantsType;

/**
 * <b>Apartment		</b> <br/> <br/>
 * 
 * Apartment details and information about all type of services offered by apartments for rentees
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 03-Mar-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */

@Entity
@Table(name = "RH_APARTMENT")
public class Apartment extends	AbstractEntity	{

	private static final long serialVersionUID = 1L;

	private String name;
	private Date constructedOn;
	private Date postedOn;
	private TenantsType tenant;
	private Integer buildUpArea;
	private RentHomeSpace bhkLength;
	private Furnished furnished;

	private Boolean nonVegAllowed;
	private Boolean petsAllowed;
	private ParkingSpace parking;
	private Boolean gardenCityAvailable;
	private Boolean swimmingPoolAvailable;
	private Boolean underCCTVServilence;
	private Boolean securityGuardianAreExist;
	private Boolean functionHallExist;

	private Long rent;
	private Long deposit;
	private Boolean negotiable;
	private Boolean depositRefundable;	
	private Integer maintainanceCharge;
	private Integer agreemantDuriation;	
	private short leavingIntimationBefore;

	private AddressSpace addressSpace;

	private List<ApartmentPictures> apartmentPictures;

	/**
	 * default constructor
	 */
	public Apartment() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the bhkLength
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "bhk_length")
	public RentHomeSpace getBhkLength() {
		return bhkLength;
	}

	/**
	 * @param bhkLength
	 */
	public void setBhkLength(RentHomeSpace bhkLength) {
		this.bhkLength = bhkLength;
	}

	/**
	 * @return the furnished
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "furnished")
	public Furnished getFurnished() {
		return furnished;
	}

	/**
	 * @param furnished the furnished to set
	 */
	public void setFurnished(Furnished furnished) {
		this.furnished = furnished;
	}

	/**
	 * @return the gardenCityAvailable
	 */
	@Type(type = "true_false")
	@Column(name = "garden_city")
	public Boolean isGardenCityAvailable() {
		return gardenCityAvailable;
	}

	/**
	 * @param gardenCityAvailable the gardenCityAvailable to set
	 */
	public void setGardenCityAvailable(Boolean gardenCityAvailable) {
		this.gardenCityAvailable = gardenCityAvailable;
	}

	/**
	 * @return the swimmingPoolAvailable
	 */
	@Type(type = "true_false")
	@Column(name = "swimming_pool")
	public Boolean isSwimmingPoolAvailable() {
		return swimmingPoolAvailable;
	}

	/**
	 * @param swimmingPoolAvailable the swimmingPoolAvailable to set
	 */
	public void setSwimmingPoolAvailable(Boolean swimmingPoolAvailable) {
		this.swimmingPoolAvailable = swimmingPoolAvailable;
	}

	/**
	 * @return the underCCTVServilence
	 */
	@Type(type = "true_false")
	@Column(name = "cc_tv_servilence")
	public Boolean isUnderCCTVServilence() {
		return underCCTVServilence;
	}

	/**
	 * @param underCCTVServilence the underCCTVServilence to set
	 */
	public void setUnderCCTVServilence(Boolean underCCTVServilence) {
		this.underCCTVServilence = underCCTVServilence;
	}

	/**
	 * @return the guardianAreExist
	 */
	@Type(type = "true_false")
	@Column(name = "security_system")
	public Boolean isSecurityGuardianAreExist() {
		return securityGuardianAreExist;
	}

	/**
	 * @param guardianAreExist the guardianAreExist to set
	 */
	public void setSecurityGuardianAreExist(Boolean securityGuardianAreExist) {
		this.securityGuardianAreExist = securityGuardianAreExist;
	}

	/**
	 * @return the functionHallExist
	 */
	@Type(type = "true_false")
	@Column(name = "function_hall")
	public Boolean isFunctionHallExist() {
		return functionHallExist;
	}

	/**
	 * @param functionHallExist the functionHallExist to set
	 */
	public void setFunctionHallExist(Boolean functionHallExist) {
		this.functionHallExist = functionHallExist;
	}

	/**
	 * @return the addressSpace
	 */
	@OneToOne (cascade = CascadeType.ALL)
	public AddressSpace getAddressSpace() {
		return this.addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(AddressSpace addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the apartmentPictures
	 */
	@OneToMany(
		cascade = CascadeType.ALL, 
		fetch = FetchType.LAZY, 
		mappedBy = "apartment"
	)
	@JsonManagedReference
	public List<ApartmentPictures> getApartmentPictures() {
		return apartmentPictures;
	}

	/**
	 * @param apartmentPictures the apartmentPictures to set
	 */
	public void setApartmentPictures(List<ApartmentPictures> apartmentPictures) {
		this.apartmentPictures = apartmentPictures;
	}

	/**
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the constructedOn
	 */
	@Column(name = "doc")
	public Date getConstructedOn() {
		return constructedOn;
	}

	/**
	 * @param constructedOn the constructedOn to set
	 */
	public void setConstructedOn(Date constructedOn) {
		this.constructedOn = constructedOn;
	}

	/**
	 * @return the postedOn
	 */
	@Column(name = "posted_on")
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * @param postedOn the postedOn to set
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	/**
	 * @return the tenant
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "tenant_type")
	public TenantsType getTenant() {
		return tenant;
	}

	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(TenantsType tenant) {
		this.tenant = tenant;
	}

	/**
	 * @return the buildUpArea
	 */
	@Column(name = "buildup_area")
	public Integer getBuildUpArea() {
		return buildUpArea;
	}

	/**
	 * @param buildUpArea the buildUpArea to set
	 */
	public void setBuildUpArea(Integer buildUpArea) {
		this.buildUpArea = buildUpArea;
	}

	/**
	 * @return the nonVegAllowed
	 */
	@Type(type = "true_false")
	@Column(name = "non_veg_allowed")
	public Boolean isNonVegAllowed() {
		return nonVegAllowed;
	}

	/**
	 * @param nonVegAllowed the nonVegAllowed to set
	 */
	public void setNonVegAllowed(Boolean nonVegAllowed) {
		this.nonVegAllowed = nonVegAllowed;
	}

	/**
	 * @return the petsAllowed
	 */
	@Type(type = "true_false")
	@Column(name = "pet_allowed")
	public Boolean isPetsAllowed() {
		return petsAllowed;
	}

	/**
	 * @param petsAllowed the petsAllowed to set
	 */
	public void setPetsAllowed(Boolean petsAllowed) {
		this.petsAllowed = petsAllowed;
	}

	/**
	 * @return the parking
	 */
	@Enumerated(value = EnumType.STRING)
	@Column(name = "parking")
	public ParkingSpace getParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(ParkingSpace parking) {
		this.parking = parking;
	}

	/**
	 * @return the rent
	 */
	@Column(name = "rent")
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the deposit
	 */
	@Column(name = "deposit")
	public Long getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(Long deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the negotiable
	 */
	@Type(type = "true_false")
	@Column(name = "negotiable")
	public Boolean isNegotiable() {
		return negotiable;
	}

	/**
	 * @param negotiable the negotiable to set
	 */
	public void setNegotiable(Boolean negotiable) {
		this.negotiable = negotiable;
	}

	/**
	 * @return the depositRefundable
	 */
	@Type(type = "true_false")
	@Column(name = "deposit_refundable")
	public Boolean isDepositRefundable() {
		return depositRefundable;
	}

	/**
	 * @param depositRefundable the depositRefundable to set
	 */
	public void setDepositRefundable(Boolean depositRefundable) {
		this.depositRefundable = depositRefundable;
	}

	/**
	 * @return the maintainanceCharge
	 */
	@Column(name = "maintainance_charge")
	public Integer getMaintainanceCharge() {
		return maintainanceCharge;
	}

	/**
	 * @param maintainanceCharge the maintainanceCharge to set
	 */
	public void setMaintainanceCharge(Integer maintainanceCharge) {
		this.maintainanceCharge = maintainanceCharge;
	}

	/**
	 * @return the agreemantDuriation
	 */
	@Column(name = "agreement_duration")
	public Integer getAgreemantDuriation() {
		return agreemantDuriation;
	}

	/**
	 * @param agreemantDuriation the agreemantDuriation to set
	 */
	public void setAgreemantDuriation(Integer agreemantDuriation) {
		this.agreemantDuriation = agreemantDuriation;
	}

	/**
	 * @return the leavingIntimationBefore
	 */
	@Column(name = "leaving_intimation_before")
	public short getLeavingIntimationBefore() {
		return leavingIntimationBefore;
	}

	/**
	 * @param leavingIntimationBefore the leavingIntimationBefore to set
	 */
	public void setLeavingIntimationBefore(short leavingIntimationBefore) {
		this.leavingIntimationBefore = leavingIntimationBefore;
	}
}