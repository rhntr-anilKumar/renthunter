package com.rhntr.core.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "RH_PG_PICTURES")
public class PGPictures	extends FilesList	{

	
	private PayingGuest payingGuest;
	
	/**
	 * default constructor
	 */
	public PGPictures() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the payingGuest
	 */
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	public PayingGuest getPayingGuest() {
		return payingGuest;
	}

	/**
	 * @param payingGuest the payingGuest to set
	 */
	public void setPayingGuest(PayingGuest payingGuest) {
		this.payingGuest = payingGuest;
	}	
}