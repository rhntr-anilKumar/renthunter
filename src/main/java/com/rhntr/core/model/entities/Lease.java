package com.rhntr.core.model.entities;

/**
 * <b>Lease		</b> <br/> <br/>
 * 
 * Leases for all type of rent space that can be hosted depending on owner interest
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 03-Mar-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */

public class Lease implements java.io.Serializable {

	
	private static final long serialVersionUID = 1L;

	/**
	 * Still under requirement forming...
	 */
	
	/**
	 * default constructor
	 * @throws Exception 
	 */
	public Lease() throws Exception {
		throw new Exception("Ex : Still under requirement forming...");
	}
}