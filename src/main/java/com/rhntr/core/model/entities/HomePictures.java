package com.rhntr.core.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "RH_HOME_PICTURES")
public class HomePictures extends FilesList	{
	
	private Home home;

	
	/**
	 * default constructor
	 */
	public HomePictures() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the home
	 */
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	public Home getHome() {
		return home;
	}

	/**
	 * @param home the home to set
	 */
	public void setHome(Home home) {
		this.home = home;
	}	
}