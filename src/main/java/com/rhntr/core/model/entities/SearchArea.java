package com.rhntr.core.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

/**
 * Rent Hunter the Home Appliance Service
 * SearchArea.java
 * Date 	:	12/11/2017
 * Class	:	Entity Class
 * Purpose	:	Base table for search map for any location on global with co ordiation with Google Map.
 *
 * @version 	1.0
 * @author 		Anil Kumar
 * 
 */

@Entity
@Table(name = "SEARCH_AREA")
public class SearchArea implements java.io.Serializable{


	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longitude ;

	// full address of site
	@Column(name = "site_address", nullable=false)
	private String siteAddress;
	
	// type : home, rent home, PG, shop, let see any more
	@Column(name = "site_type", nullable=false)
	private String siteType;

	// girls or boys
	@Column(name = "pg_type", nullable=false)
	private char typeOfPG;

	// with food or without food'
	@Type(type = "true_false")
	@Column(name = "food_service")
	private Boolean foodService;

	// like WI-FI
	@Column(name = "addOnServices")
	private String addOnServices;
	
	/*@ManyToOne*/
	//private User user;

	/**
	 * @return the user
	 */

/*	@JsonIgnore
	@ManyToOne
	
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	/*public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the foodService
	 */
	public Boolean getFoodService() {
		return foodService;
	}

	public SearchArea() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the siteAddress
	 */
	public String getSiteAddress() {
		return siteAddress;
	}

	/**
	 * @param siteAddress the siteAddress to set
	 */
	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	/**
	 * @return the siteType
	 */
	public String getSiteType() {
		return siteType;
	}

	/**
	 * @param siteType the siteType to set
	 */
	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	/**
	 * @return the typeOfPG
	 */
	public char getTypeOfPG() {
		return typeOfPG;
	}

	/**
	 * @param typeOfPG the typeOfPG to set
	 */
	public void setTypeOfPG(char typeOfPG) {
		this.typeOfPG = typeOfPG;
	}

	/**
	 * @return the foodService
	 */
	public Boolean isFoodService() {
		return foodService;
	}

	/**
	 * @param foodService the foodService to set
	 */
	public void setFoodService(Boolean foodService) {
		this.foodService = foodService;
	}

	/**
	 * @return the addOnServices
	 */
	public String getAddOnServices() {
		return addOnServices;
	}

	/**
	 * @param addOnServices the addOnServices to set
	 */
	public void setAddOnServices(String addOnServices) {
		this.addOnServices = addOnServices;
	}
}
