package com.rhntr.core.model.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.rhntr.core.enums.FoodType;
import com.rhntr.core.enums.PGType;

/**
 * <b>PayingGuest		</b> <br/> <br/>
 * 
 * PayingGuest is record of all registered PG's for their online hosting of hospitality.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 05-Jan-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		UJ Madhu
 * @see			{@link PGSpaceProperties}
 * 
 */

@Entity
@Table(name = "RH_PG_SPACE")
public class PayingGuest	extends	AbstractEntity	{

	private static final long serialVersionUID = 1L;

	private PGType type;

	private String name;
	
	private FoodType foodType;
	
	private Integer availableRooms;
	
	private Integer totalRooms;

	private Integer advance;
	
	private Boolean parking;
	
	private Boolean refundable;
	
	private Boolean negotiable;
	
	private Boolean underCCTVServilence;

	private Boolean securityGuardianAreExist;
	
	private List<PGSpaceProperties> spaceProperties;
 
	private AddressSpace addressSpace;
	
	private Date postedOn;

	private List<PGPictures> pgPictures;
	/**
	 * default constructor
	 */
	public PayingGuest() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the type
	 */
	@Enumerated(value = EnumType.STRING)
	@Column(name = "pg_type", nullable = false)
	public PGType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(PGType type) {
		this.type = type;
	}

	/**
	 * @return the pgName
	 */
	@Column(name = "pg_name")
	public String getName() {
		return name;
	}

	/**
	 * @param pgName the pgName to set
	 */
	public void setName(String pgName) {
		this.name = pgName;
	}

	/**
	 * @return the foodType
	 */
	@Enumerated(value = EnumType.ORDINAL)
	@Column(name = "food_type")
	public FoodType getFoodType() {
		return foodType;
	}

	/**
	 * @param foodType the foodType to set
	 */
	public void setFoodType(FoodType foodType) {
		this.foodType = foodType;
	}

	/**
	 * @return the availableRooms
	 */
	@Column(name = "availble_rooms")
	public Integer getAvailableRooms() {
		return availableRooms;
	}

	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(Integer availableRooms) {
		this.availableRooms = availableRooms;
	}

	/**
	 * @return the totalRooms
	 */
	@Column(name = "total_rooms")
	public Integer getTotalRooms() {
		return totalRooms;
	}

	/**
	 * @param totalRooms the totalRooms to set
	 */
	public void setTotalRooms(Integer totalRooms) {
		this.totalRooms = totalRooms;
	}

	/**
	 * @return the advance
	 */
	@Column(name = "advance")
	public Integer getAdvance() {
		return advance;
	}

	/**
	 * @param advance the advance to set
	 */
	public void setAdvance(Integer advance) {
		this.advance = advance;
	}

	/**
	 * @return the underCCTVServilence
	 */
	@Type(type = "true_false")
	@Column(name = "cc_tv")
	public Boolean isUnderCCTVServilence() {
		return underCCTVServilence;
	}

	/**
	 * @param underCCTVServilence the underCCTVServilence to set
	 */
	public void setUnderCCTVServilence(Boolean underCCTVServilence) {
		this.underCCTVServilence = underCCTVServilence;
	}

	/**
	 * @return the securityGuardianAreExist
	 */
	@Type(type = "true_false")
	@Column(name = "security_guard")
	public Boolean isSecurityGuardianAreExist() {
		return securityGuardianAreExist;
	}

	/**
	 * @param securityGuardianAreExist the securityGuardianAreExist to set
	 */
	public void setSecurityGuardianAreExist(Boolean securityGuardianAreExist) {
		this.securityGuardianAreExist = securityGuardianAreExist;
	}

	/**
	 * @return the spaceProperties
	 */
	@JsonManagedReference
	@OneToMany(
			cascade = CascadeType.ALL, 
			fetch = FetchType.EAGER, 
			mappedBy = "payingGuest"
		)
	public List<PGSpaceProperties> getSpaceProperties() {
		return spaceProperties;
	}

	/**
	 * @param spaceProperties the spaceProperties to set
	 */
	public void setSpaceProperties(List<PGSpaceProperties> spaceProperties) {
		this.spaceProperties = spaceProperties;
	}

	/**
	 * @return the addressSpace
	 */
	@OneToOne(cascade = CascadeType.ALL)
	public AddressSpace getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(AddressSpace addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the parking
	 */
	@Type(type = "true_false")
	@Column(name = "parking")
	public Boolean isParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(Boolean parking) {
		this.parking = parking;
	}

	/**
	 * @return the refundable
	 */
	@Type(type = "true_false")
	@Column(name = "refundable")
	public Boolean isRefundable() {
		return refundable;
	}

	/**
	 * @param refundable the refundable to set
	 */
	public void setRefundable(Boolean refundable) {
		this.refundable = refundable;
	}

	/**
	 * @return the negotiable
	 */
	@Type(type = "true_false")
	@Column(name = "negotiable")
	public Boolean isNegotiable() {
		return negotiable;
	}

	/**
	 * @param negotiable the negotiable to set
	 */
	public void setNegotiable(Boolean negotiable) {
		this.negotiable = negotiable;
	}

	/**
	 * @return the pgPictures
	 */
	@OneToMany(
		cascade = CascadeType.ALL, 
		fetch = FetchType.LAZY, 
		mappedBy = "payingGuest"
	)
	@JsonManagedReference
	public List<PGPictures> getPgPictures() {
		return pgPictures;
	}

	/**
	 * @param pgPictures the pgPictures to set
	 */
	public void setPgPictures(List<PGPictures> pgPictures) {
		this.pgPictures = pgPictures;
	}
	
	/**
	 * @return the postedOn
	 */
	@Column(name = "posted_on")
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * @param postedOn the postedOn to set
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}
}