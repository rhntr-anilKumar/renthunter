package com.rhntr.core;

/**
 * <b>Params	</b> <br/> <br/>
 * 
 * Param is constants class for setting global name for intermediate 
 * object transformation among multiple calls.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Constants Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class Params {

	private Params() {
		throw new UnsupportedOperationException();
	}
	
	public static final String USER_OBJECT = "user_obj";
	
	public static final String USERS_LIST = "users_list";
	
	public static final String USERS_MAP = "users_map";
	
	public static final String USER_ADDRESS_OBJECT = "address_obj";
	
	public static final String PG_OBJECT = "pg_object";

	public static final String GOOGLED_ADDRESS_OBJECT = "googled_add_object";
	
	
	/* Generic reference name for referring response data */
	
	public static final String INPUT = "input";
	
	public static final String NEW_ENTRY = "newEntry";
	
	public static final String OLD_ENTRY = "oldEntry";
	
	public static final String UPDATED_ENTRY = "updatedEntry";
	
	public static final String DELETED_ENTRY = "deletedEntry";	
	
	
	
	public static final String ADDRESS_MAP = "addressMap";
	
	public static final String PROPERTIES = "properties";
	
	public static final String GOOGLE_ADDRESS_IDS = "googledAddressIds";
	
	
	
}
