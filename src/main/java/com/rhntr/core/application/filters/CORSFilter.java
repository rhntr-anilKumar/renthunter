package com.rhntr.core.application.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rhntr.core.Constants;
import com.rhntr.core.application.launcher.RentHunter.ApplicationBeanConfiguration;

/**
 * <b>CORSFilter </b>  <br/> <br/> <br/>
 * 
 * <b>Cross-Origin Resource Sharing</b>
 * <p>
 * 		Changing the responce origin to request origin type,<br/>
 * 		if request raised from different domain
 * </p>
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 23-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Servlet Filter <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link Filter}, {@link FilterChain}, {@link ApplicationBeanConfiguration}
 * 
 */
public class CORSFilter implements Filter{

	private Logger log = LoggerFactory.getLogger(CORSFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		log.info("Intialising CORS Filter....");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest _request = (HttpServletRequest)request;
		HttpServletResponse _response = (HttpServletResponse)response;
		
		log.info("Request @ " + _request.getRequestURL());

		String reqOrigin = _request.getHeader(Constants.HEADER_Origin);
		if(reqOrigin != null)	{
			log.info("Request from : " + reqOrigin);
			_response.setHeader(Constants.HEADER_Access_Control_Allow_Origin, reqOrigin);
		}else{
			_response.setHeader(Constants.HEADER_Access_Control_Allow_Origin, "*");
		}
		_response.setHeader(Constants.HEADER_Access_Control_Allow_Methods, "*");
		//_response.setHeader(RequestUtil.HEADER_ACCESS_CONTROL_ALLOW_METHODS, "GET, POST, PUT, OPTIONS, DELETE");
		
		_response.setHeader(Constants.HEADER_Access_Control_Allow_Headers, "*");
		/*_response.setHeader(Constants.HEADER_Access_Control_Allow_Headers, 
				Constants.HEADER_Origin + ", " + Constants.HEADER_X_Requested_With + ", " 
				+ Constants.HEADER_Content_Type + ", " + Constants.HEADER_Accept
				+ ", " + Constants.HEADER_X_Auth_Token + ", " + Constants.HEADER_X_Csrf_Token
				+ ", " + Constants.HEADER_Authorization);*/
		
		chain.doFilter(_request, _response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub		
	}
}
