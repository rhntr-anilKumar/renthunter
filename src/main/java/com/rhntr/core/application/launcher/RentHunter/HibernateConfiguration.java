package com.rhntr.core.application.launcher.RentHunter;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <b>HibernateConfiguration	</b> <br/> <br/>
 * 
 * Data layer configuration using Hibernate ORM.
 * Loading ORACLE JDBC driver using spring application property file.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-Jan-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Layer Configuration <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link DataSource}, {@link LocalSessionFactoryBean}, {@link HibernateTransactionManager}
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.rhntr.core.model"})
public class HibernateConfiguration {

	@Value("${spring.datasource.driver-class-name}")
	private String DRIVER;

	@Value("${spring.datasource.password}")
	private String PASSWORD;

	@Value("${spring.datasource.url}")
	private String URL;

	@Value("${spring.datasource.username}")
	private String USERNAME;

	@Value("${spring.jpa.properties.hibernate.dialect}")
	private String DIALECT;

	@Value("${spring.jpa.show-sql}")
	private String SHOW_SQL;

	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String HBM2DDL_AUTO;

	@Value("${spring.jpa.properties.packages2scan}")
	private String PACKAGES_TO_SCAN;

	/**
	 * JDBC data-source configuration
	 * 
	 * @return {@link DataSource}
	 */
	@Bean
	public DataSource dataSource() {

		DriverManagerDataSource dataSource = new 
				DriverManagerDataSource(URL, USERNAME, PASSWORD);
		dataSource.setDriverClassName(DRIVER);
		return dataSource;
	}

	/**
	 * Configuring entity manager and hibernate jpa adapter
	 * 
	 * @return	{@link EntityManagerFactory}, {@link LocalContainerEntityManagerFactoryBean}, {@link HibernateJpaVendorAdapter}
	 */
	@Bean
	public EntityManagerFactory entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = 
					new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(PACKAGES_TO_SCAN);
		factory.setDataSource(dataSource());
		factory.afterPropertiesSet();

		return factory.getObject();
	}

	/**
	 * Configuring jpa platform transaction manger entity
	 * 
	 * @return		{@link PlatformTransactionManager}, {@link JpaTransactionManager}
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		return txManager;
	}

	/**
	 * Configuring session factory entity and session bean
	 * 
	 * @return {@link LocalSessionFactoryBean}, {@link SessionFactory}, {@link Session}
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(PACKAGES_TO_SCAN);
		Properties hibernateProperties = new Properties();
		hibernateProperties.put("hibernate.dialect", DIALECT);
		hibernateProperties.put("hibernate.show_sql", SHOW_SQL);
		hibernateProperties.put("hibernate.hbm2ddl.auto", HBM2DDL_AUTO);
		sessionFactory.setHibernateProperties(hibernateProperties);

		return sessionFactory;
	}

	/*@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource)	{
		LocalContainerEntityManagerFactoryBean entityManagerFactory 
								= new LocalContainerEntityManagerFactoryBean();
		//entityManagerFactory.setPersistenceUnitName("hibernate-persistence");
		entityManagerFactory.setDataSource(dataSource);
		entityManagerFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactory.setJpaDialect(new HibernateJpaDialect());
		entityManagerFactory.setPackagesToScan("com.shengwang.demo.model");

		entityManagerFactory.setJpaPropertyMap(hibernateJpaProperties());
		return entityManagerFactory;
	}

	private Map<String, String> hibernateJpaProperties()	{
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("hibernate.hbm2ddl.auto", HBM2DDL_AUTO);
		properties.put("hibernate.show_sql", SHOW_SQL);
		properties.put("hibernate.format_sql", "false");
		//properties.put("hibernate.hbm2ddl.import_files", "insert-data.sql");
		properties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");

		properties.put("hibernate.c3p0.min_size", "2");
		properties.put("hibernate.c3p0.max_size", "5");
		properties.put("hibernate.c3p0.timeout", "300"); // 5mins

		return properties;
	}

	@Bean
	public HibernateTransactionManager hibernateTransactionManager()	{
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory().getObject());
		return transactionManager;
	}*/
}