package com.rhntr.core.application.launcher.RentHunter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * <b>ServletInitializer	</b> <br/> <br/>
 * 
 * Application Boot Startup in any application server,
 * now running in spring default server.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 23-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Configuration & Start up <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link SpringBootServletInitializer}, {@link SpringApplicationBuilder}, {@link RentHunterApplication}
 */
public class ServletInitializer extends SpringBootServletInitializer {

	private Logger log = LoggerFactory.getLogger(ServletInitializer.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)	{
		
		log.info("Initialising Servlet Context...");
		
		return application.sources(RentHunterApplication.class);
	}
}
