package com.rhntr.core.application.launcher.RentHunter;

import java.lang.reflect.Method;
import java.util.Arrays;

import javax.servlet.Filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.rhntr.core.ApplicationConfig;
import com.rhntr.core.application.filters.CORSFilter;

/**
 * <b>ApplicationBeanConfiguration		</b> <br/> <br/>
 * 
 * Addition bean configuration using Java Based Bean Configuration.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-Jan-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Configuration & Start up <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link FilterRegistrationBean}, {@link Filter}, {@link CORSFilter}
 */
@Configuration
public class ApplicationBeanConfiguration {

	private Logger log = LoggerFactory.getLogger(ApplicationBeanConfiguration.class);

	/**
	 * Registering CORS Filter in application
	 * 
	 * @return		{@link FilterRegistrationBean}
	 */
	@Bean
	public FilterRegistrationBean corsFilterRegistration()	{
		FilterRegistrationBean corsFilterBean = new FilterRegistrationBean();
		corsFilterBean.setFilter(new CORSFilter());
		corsFilterBean.setName("CORS Servlet Filter");
		corsFilterBean.setUrlPatterns(Arrays.asList("/*"));
		corsFilterBean.setOrder(1);
		return corsFilterBean;
	}

	/**
	 * Adding prefix/base path for REST API.
	 * 
	 * @return	{@link WebMvcRegistrationsAdapter}
	 */
	@Bean
    public WebMvcRegistrationsAdapter webMvcRegistrationsHandlerMapping() {
		
		log.info("ApplicationConfig.REST_API_BASE_PATH : " + ApplicationConfig.REST_API_BASE_PATH);
		
        return new WebMvcRegistrationsAdapter() {
            @Override
            public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
                return new RequestMappingHandlerMapping() {
                	// setting base path for rest controller
                    @Override
                    protected void registerHandlerMethod(Object handler, Method method, RequestMappingInfo mapping) {
                    	
                        Class<?> beanType = method.getDeclaringClass();
                        // only getting bean of type annotated as RestController.
                        RestController restApiController = beanType.getAnnotation(RestController.class);
                        
                        if (restApiController != null) {
                        	
                            PatternsRequestCondition apiPattern = new PatternsRequestCondition(
                            		ApplicationConfig.REST_API_BASE_PATH)
                            		.combine(mapping.getPatternsCondition());

                            mapping = new RequestMappingInfo(mapping.getName(), apiPattern,
                                    mapping.getMethodsCondition(), mapping.getParamsCondition(),
                                    mapping.getHeadersCondition(), mapping.getConsumesCondition(),
                                    mapping.getProducesCondition(), mapping.getCustomCondition());
                        }

                        super.registerHandlerMethod(handler, method, mapping);
                    }
                };
            }
        };
    }
}
