package com.rhntr.core.application.launcher.RentHunter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * <b>RentHunterApplication		</b> <br/> <br/>
 * 
 * Application Boot Startup
 * 
 * <br/> <br/> <br/>0
 * 
 * <b>Date 		: </b> 23-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Configuration & Start up <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see 		{@link ServletInitializer}, {@link ApplicationBeanConfiguration}, {@link HibernateConfiguration}
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages="com.rhntr")
public class RentHunterApplication {
	
	private static Logger log = LoggerFactory.getLogger(RentHunterApplication.class);

	public static void main(String ...args)	{

		log.info("SpringApplication : RentHunterApplication started...");
		SpringApplication.run(RentHunterApplication.class, args);
	}
}