package com.rhntr.core;

/**
 * <b>Constants		</b> <br/> <br/>
 * 
 * Application specific constants values.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 26-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Constants Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class Constants {

	/**
	 * Singleton object...
	 */
	private Constants() {
		throw new UnsupportedOperationException();
	}

	/** Empty String */
	public static final String EMPTY_STRING = new String("");
	
	/** Boolean true/false for string manipulation */
	public static final String BOOLEAN_TRUE = Boolean.TRUE.toString();
	public static final String BOOLEAN_FALSE = Boolean.FALSE.toString();

	/** User interaction with application by type of input */
	public static final String INPUT_TYPE_TEXT_BOX = "i";
	public static final String INPUT_TYPE_MAP = "m";
	public static final String INPUT_TYPE_CHECK_BOX = "cb";
	public static final String INPUT_TYPE_RADIO_BOX = "rb";
	public static final String INPUT_TYPE_SELECT = "select";
	

	/** Request/Response Header Types */
	public static final String HEADER_Origin = "Origin";
	public static final String HEADER_Accept = "Accept";
	public static final String HEADER_Content_Type = "Content-Type";
	public static final String HEADER_X_Auth_Token = "X-Auth-Token";
	public static final String HEADER_X_Csrf_Token = "X-Csrf-Token";
	public static final String HEADER_Authorization = "Authorization";
	public static final String HEADER_X_Requested_With = "X-Requested-With";
	
	
	public static final String HEADER_Access_Control_Allow_Origin = "Access-Control-Allow-Origin";
	public static final String HEADER_Access_Control_Allow_Methods = "Access-Control-Allow-Methods";
	public static final String HEADER_Access_Control_Allow_Headers = "Access-Control-Allow-Headers";
	public static final String HEADER_Access_Control_Allow_Credentials = "Access-Control-Allow-Credentials";
	public static final String HEADER_Access_Control_Max_Age = "Access-Control-Max-Age";

	public static final char PG_TYPE_GIRLS = 'g';
	public static final char PG_TYPE_BOYS = 'b';

	public static final Float DEFAULT_LATITUDE = -1.0F;

	public static final Float DEFAULT_LONGITUDE = -1.0F;
	
	public static final Long LONG_ZERO = new Long(0);

	// Empty Count for all basic data types.
	public static final String EMPTY = new String();
	public static final Long EMPTY_COUNT_L = new Long(0);
	public static final Float EMPTY_COUNT_F = new Float(0);
	public static final Double EMPTY_COUNT_D = new Double(0);
	public static final Integer EMPTY_COUNT = new Integer(0);
}
