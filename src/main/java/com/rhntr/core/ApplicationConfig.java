package com.rhntr.core;

/**
 * <b>ApplicationConfig		</b> <br/> <br/>
 * 
 * Application configuration property information.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 26-Dec-2017 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Constants Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class ApplicationConfig	{


	/**	REST API calls base path, entry point url	*/
	public final static String REST_API_BASE_PATH = "/rhntr/api";
	
	public final static String FILE_UPLOAD_DIRECTORY = "/home/renthunt/proprty-pics";
}