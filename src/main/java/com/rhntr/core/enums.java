package com.rhntr.core;

/**
 * <b>enums		</b> <br/> <br/>
 * 
 * Enumeration class for specifying predefined constant values 
 * for some user defined data types and allowing new types of 
 * data representation for object entities.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-Jan-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Custom Data Types <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class enums {

	public static enum Gender {
		MALE("M"), FEMALE("F"), OTHERS("O");
		private String val;
		private Gender(final String val)	{
			this.val = val;
		}
	}
	
	public static enum PGType {
		BOYS, GIRLS
	}
	
	public static enum PropertyType {
		HOME, PG, APARTMENT, SHOPS, OFFICE, REAT_ESTATE, WAREHOUSE,
		FUNCTION_HALL // Future plan for booking function hall
	}
	
	public static enum Availability	{
		NOT_AVAILABLE, GOINT_TO_VACANT, AVAILABLE
	}
	
	public static enum RentHomeSpace	{
		SINGLE_ROOM, RK, ONE_BHK, TWO_BHK, THREE_BHK, FOUR_BHK, ANY_BHK, VILLA
	}
	
	public static enum FoodType	{
		WITH_FOOD, WITHOUT_FOOD
	}
	
	public static enum PGSharingType {
		SINGLE_SHARING, DOUBLE_SHARING, THREE_SHARING, FOUR_SHARING, FIVE_SHARING, OPEN_SHARING, ANY_SHARING
	}
	
	public static enum Furnished	{
		UNFURNISHED, SEMI_FURNISHED, FULL_FURNISHED, ON_DEMAND_FURNISHED
	}
	
	public static enum TenantsType	{
		BACHELORS, FAMILY, ANY
	}
	
	public static enum ParkingSpace {
		BIKE, CAR, BOTH
	}
}