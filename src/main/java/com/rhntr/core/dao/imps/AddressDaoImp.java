package com.rhntr.core.dao.imps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.rhntr.core.dao.AddressDao;

/**
 * <b>AddressDao	</b> <br/> <br/>
 * 
 * AddressDao provides address registration/persistence functionalities.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Access Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AddressDao}, {@link AbstractBaseDao}
 * 
 */
@Repository
public class AddressDaoImp extends AbstractBaseDao implements AddressDao	{
	

	private Logger log = LoggerFactory.getLogger(UserDaoImp.class);

	/**
	 * default constructor,
	 */
	public AddressDaoImp() {
		// TODO Auto-generated constructor stub
	}
}