package com.rhntr.core.dao.imps;

/**
 * <b>AbstractBaseDao	</b> <br/> <br/>
 * 
 * AbstractBaseDao provides common functionalities among multiple dao's implementation.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Access Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public abstract class AbstractBaseDao {

	/**
	 * default constructor,
	 */
	public AbstractBaseDao() {
		// TODO Auto-generated constructor stub
	}
}
