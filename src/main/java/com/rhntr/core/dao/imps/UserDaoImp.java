package com.rhntr.core.dao.imps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.rhntr.core.dao.UserDao;

/**
 * <b>UserDao	</b> <br/> <br/>
 * 
 * UserDao provides rentee/owner registration/persistence functionalities.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Access Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link UserDao}, {@link AbstractBaseDao}
 * 
 */
@Repository
public class UserDaoImp extends AbstractBaseDao implements UserDao	{

	private Logger log = LoggerFactory.getLogger(UserDaoImp.class);

	/**
	 * default constructor,
	 */
	public UserDaoImp() {
		
	}
}