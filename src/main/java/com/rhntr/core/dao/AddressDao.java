package com.rhntr.core.dao;

/**
 * <b>AddressDao	</b> <br/> <br/>
 * 
 * AddressDao provides address registration/persistence functionalities.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Access Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public interface AddressDao {

}