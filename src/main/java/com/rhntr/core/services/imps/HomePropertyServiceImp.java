package com.rhntr.core.services.imps;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.HomeDTO;
import com.rhntr.core.enums.Availability;
import com.rhntr.core.enums.PropertyType;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.GoogledAddress;
import com.rhntr.core.model.entities.Home;
import com.rhntr.core.model.entities.HomePictures;
import com.rhntr.core.model.entities.HomeSpaceProperties;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.GAddressRepository;
import com.rhntr.core.model.repositories.HomeRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.HomePropertyService;
import com.rhntr.utils.DateAndTimeUtil;
import com.rhntr.utils.ModelMapperUtil;

@Service
@Transactional
public class HomePropertyServiceImp implements HomePropertyService	{

	private Logger log = LoggerFactory.getLogger(HomePropertyServiceImp.class);

	@Autowired
	private GAddressRepository googleAddressRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private HomeRepository homeRepo;
	
	/**
	 * @param homeDTO
	 * @return
	 * @throws Exception
	 */
	@Override
	public Home registeringNewHomeProperty(HomeDTO homeDTO) throws Exception {

		log.debug("METHOD::registeringNewHomeProperty(HomeDTO homeDTO)");

		GoogledAddress googledAddress = googleAddressRepo.getByPlaceIdAndFormattedAddress(homeDTO
				.getAddressSpace().getPlaceId(), homeDTO.getAddressSpace().getFormattedAddress());
		
		if(googledAddress == null)	{
			googledAddress = googleAddressRepo.saveAndFlush(new ModelMapper()
					.map(homeDTO.getAddressSpace(), GoogledAddress.class));
			log.info("New Google address added.[" + googledAddress.getPlaceId() 
				+ " :: " + googledAddress.getFormattedAddress() + "]");
		} else {
			log.info("New property @location : " + googledAddress.getFormattedAddress());
		}

		User user = userRepo.getOne(homeDTO.getUser().getId());
		log.info("Property user " + user.getFirstName() + ", @id " + user.getId());

		AddressSpace addressSpace = new AddressSpace();
		addressSpace.setPropertyType(PropertyType.HOME);
		addressSpace.setUser(user);
		addressSpace.setGoogledAddress(googledAddress);

		HomeSpaceProperties spaceProperties = (HomeSpaceProperties) 
				ModelMapperUtil.map(homeDTO, HomeSpaceProperties.class);
		spaceProperties.setAvailability(Availability.AVAILABLE);
		
		Home newProperty = new ModelMapper().map(homeDTO, Home.class);
		newProperty.setPostedOn(DateAndTimeUtil.currentDate());
		newProperty.setAddressSpace(addressSpace);
		
		newProperty.setSpaceProperties(spaceProperties);
		spaceProperties.setHome(newProperty);
		for(HomePictures homePictures : newProperty.getHomePictures())	{
			homePictures.setHome(newProperty);
		}
		
		newProperty = homeRepo.saveAndFlush(newProperty);
				
		log.info("home property id " + newProperty.getId());
		return newProperty;
	}

	@Override
	public List<HomeDTO> recentPostedProperty() throws Exception {

		return homeRepo.recentPostedProperty()
				.stream().limit(5)
				.collect(Collectors.toList());
	}
}