package com.rhntr.core.services.imps;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.PgDTO;
import com.rhntr.core.enums.PropertyType;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.GoogledAddress;
import com.rhntr.core.model.entities.PGPictures;
import com.rhntr.core.model.entities.PGSpaceProperties;
import com.rhntr.core.model.entities.PayingGuest;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.GAddressRepository;
import com.rhntr.core.model.repositories.PgRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.PgPropertyServices;
import com.rhntr.utils.DateAndTimeUtil;

@Service
@Transactional
public class PgPropertyServiceImp	implements	PgPropertyServices	{

	private Logger log = LoggerFactory.getLogger(PgPropertyServiceImp.class);

	@Autowired
	private GAddressRepository googleAddressRepo;
	
	@Autowired
	private PgRepository pgRepo;
	
	@Autowired
	private UserRepository userRepo;

	@Override
	public PayingGuest registeringNewPgProperty(PgDTO pgDTO) throws Exception {

		log.debug("METHOD::registeringNewPgProperty(PgDTO pgDTO)");

		GoogledAddress googledAddress = googleAddressRepo.getByPlaceIdAndFormattedAddress(pgDTO
				.getAddressSpace().getPlaceId(), pgDTO.getAddressSpace().getFormattedAddress());
		
		if(googledAddress == null)	{
			googledAddress = googleAddressRepo.saveAndFlush(new ModelMapper()
					.map(pgDTO.getAddressSpace(), GoogledAddress.class));
			log.info("New Google address added.[" + googledAddress.getPlaceId() 
				+ " :: " + googledAddress.getFormattedAddress() + "]");
		} else {
			log.info("New property @location : " + googledAddress.getFormattedAddress());
		}
		
		User user = userRepo.getOne(pgDTO.getUser().getId());
		log.info("Property user " + user.getFirstName() + ", @id " + user.getId());
		
		AddressSpace addressSpace = new AddressSpace();
		addressSpace.setPropertyType(PropertyType.PG);
		addressSpace.setUser(user);
		addressSpace.setGoogledAddress(googledAddress);
		
		PayingGuest newProperty = new ModelMapper().map(pgDTO, PayingGuest.class);
		newProperty.setPostedOn(DateAndTimeUtil.currentDate());
		newProperty.setAddressSpace(addressSpace);

		// binding pg to all individual sharing's 
		for(PGSpaceProperties property : newProperty.getSpaceProperties())	{
			property.setPayingGuest(newProperty);
		}
		
		// binding pg to all pictures 
		for(PGPictures pgPicture : newProperty.getPgPictures())	{
			pgPicture.setPayingGuest(newProperty);
		}
		
		newProperty = pgRepo.saveAndFlush(newProperty);
		log.info("pg property id " + newProperty.getId());
		return newProperty;
	}
}
