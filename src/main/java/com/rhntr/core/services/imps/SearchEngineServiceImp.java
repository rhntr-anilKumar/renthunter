package com.rhntr.core.services.imps;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.AreaSearchInput;
import com.rhntr.core.Params;
import com.rhntr.core.enums.PropertyType;
import com.rhntr.core.model.entities.GoogledAddress;
import com.rhntr.core.model.repositories.ApartmentRepository;
import com.rhntr.core.model.repositories.HomeRepository;
import com.rhntr.core.model.repositories.PgRepository;
import com.rhntr.core.services.SearchEngineService;
import com.rhntr.utils.CollectionUtil;
import com.rhntr.utils.StringUtil;

/**
 * <b>SearchEngineService	</b> <br/> <br/>
 * 
 * Search Engine service provides search over addresses.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 02-May-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Service Layer <br/> <br/>
 * 
 * @author 		Anil Kumar
 * @version		1.0
 * 
 */
@Service
@Transactional
public class SearchEngineServiceImp implements SearchEngineService {

	private Logger log = LoggerFactory.getLogger(SearchEngineServiceImp.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private HomeRepository homeRepository;
	
	@Autowired
	private PgRepository pgRepository;
	
	@Autowired
	private ApartmentRepository apartmentRepository;

	/**
	 * Search functionality using google map generic input.
	 * 
	 * @param searchInput
	 * @return
	 */
	@Override
	public Map searchAddressSpace(AreaSearchInput searchInput) {

		Session session = this.sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(GoogledAddress.class);
		
		List<GoogledAddress> addressList = (List<GoogledAddress>) 
				formAddressFetchingCriteria(criteria, searchInput).list();
		
		Map<Long, GoogledAddress> addressMap = addressList.stream()
				.collect(Collectors.toMap(GoogledAddress::getId, object -> object));
		
		Set<Long> addressIds = addressMap.keySet();
		
		List propertyList = null;
		if(!CollectionUtil.isNullOrEmpty(addressIds))	{
			propertyList = getPropertyList(
					searchInput.getPropertyType(), addressIds);
		}
		Map map = new HashMap();
		if(!CollectionUtil.isNullOrEmpty(propertyList))	{
			map.put(Params.ADDRESS_MAP, addressMap);
			map.put(Params.PROPERTIES, propertyList);
			map.put(Params.GOOGLE_ADDRESS_IDS, addressIds);

			log.info("Property list size : " + propertyList.size() 
				+ ", effective Google addresses " + addressIds.size() + ".");
		} else {
			log.info("Property list not found for location : " + searchInput.getFormattedAddress());
		}
		return map;
	}

	/**
	 * 
	 * 
	 * @param propertyType
	 * @param addressIds
	 * @return
	 */
	private List getPropertyList(PropertyType propertyType, Set<Long> addressIds) {

		List propertyList = null;
		if(PropertyType.PG.equals(propertyType))	{
			propertyList = pgRepository.findByGoogledAddressIds(addressIds);
		} else if(PropertyType.HOME.equals(propertyType)){
			propertyList = homeRepository.findByGoogledAddressIds(addressIds);
		}
		return propertyList;
	}

	/**
	 * @param criteria
	 * @param searchInput
	 * @return
	 */
	private Criteria formAddressFetchingCriteria(Criteria criteria, AreaSearchInput searchInput) {
 
		Disjunction orStatements = Restrictions.disjunction();
		
		if(!StringUtil.isNullOrEmpty(searchInput.getPlaceId()))	{
			orStatements.add(Restrictions.ilike("placeId", searchInput
					.getPlaceId().toLowerCase(), MatchMode.EXACT));
		}
		if(!StringUtil.isNullOrEmpty(searchInput.getStreetNumber()))	{
			orStatements.add(Restrictions.ilike("route", searchInput.getRoute()
					.toLowerCase(), MatchMode.EXACT));
		}
		if(!StringUtil.isNullOrEmpty(searchInput.getSublocalityLevel2()))	{
			orStatements.add(Restrictions.ilike("sublocalityLevel2", searchInput
					.getSublocalityLevel2().toLowerCase(), MatchMode.EXACT));
		}
		
		// Adding most commonly observable properties as or'ed condition
		if(orStatements.conditions().iterator().hasNext())
				criteria.add(orStatements);

		if(!StringUtil.isNullOrEmpty(searchInput.getSublocalityLevel1()))	{
			criteria.add(Restrictions.ilike("sublocalityLevel1", searchInput
					.getSublocalityLevel1().toLowerCase(), MatchMode.EXACT));
		}
		if(!StringUtil.isNullOrEmpty(searchInput.getLocality()))	{
			criteria.add(Restrictions.ilike("locality", searchInput
					.getLocality().toLowerCase(), MatchMode.EXACT));
		}
		if(!StringUtil.isNullOrEmpty(searchInput.getAdministrativeAreaLevel2()))	{
			criteria.add(Restrictions.ilike("administrativeAreaLevel2", searchInput
					.getAdministrativeAreaLevel2().toLowerCase(), MatchMode.EXACT));
		}
		if(!StringUtil.isNullOrEmpty(searchInput.getAdministrativeAreaLevel1()))	{
			criteria.add(Restrictions.ilike("administrativeAreaLevel1", searchInput
					.getAdministrativeAreaLevel1().toLowerCase(), MatchMode.EXACT));
		}
		if(!StringUtil.isNullOrEmpty(searchInput.getCountry()))	{
			criteria.add(Restrictions.ilike("country", searchInput
					.getCountry().toLowerCase(), MatchMode.EXACT));
		}
		
		if(!StringUtil.isNullOrEmpty(searchInput.getPostalCode()))	{
			criteria.add(Restrictions.like("postalCode", searchInput
					.getPostalCode(), MatchMode.EXACT));
		}
		
		return criteria;
	}
}