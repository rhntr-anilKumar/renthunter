package com.rhntr.core.services.imps;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.UserInformationDTO;
import com.rhntr.core.Params;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.AbstractService;
import com.rhntr.core.services.UserMutatorService;
import com.rhntr.exceptions.BusinessFlowException;
import com.rhntr.utils.ModelMapperUtil;
import com.rhntr.utils.PasscodeUtil;

/**
 * <b>UserMutatorService		</b> <br/> <br/>
 * 
 * User mutator service provides rentee/owener related information 
 * and service for persisting any information against db.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Service Layer <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link UserMutatorService}, {@link AbstractService}
 * 
 */
@Service
@Transactional
public class UserMutatorServiceImp extends AbstractService implements UserMutatorService	{

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;
	
	/**
	 * Registering the new rentee/owner user in application.
	 * 
	 * @param registrationInfo		{@link UserInformationDTO}
	 * @return
	 * 
	 * @throws Exception
	 */
	@Override
	public UserInformationDTO registerNewUser(UserInformationDTO registrationInfo) throws Exception {

		if(userRepository.existsByEmailIdOrPhoneNumber(
				registrationInfo.getEmailId(), registrationInfo.getPhoneNumber()))	{
			log.info("user is already exists.");
			throw new BusinessFlowException("User Already Exists...");
		}else	{
			log.info("registering new user...");
			User user = (User) ModelMapperUtil.map(registrationInfo, User.class);
			user.setAddressSpaces(new ArrayList<AddressSpace>());
			user.setRegistrationOfDate(new Date(System.currentTimeMillis()));
			user.setPasscode(bcryptEncoder.encode(user.getPasscode()));

			user = userRepository.saveAndFlush(user);
			registrationInfo.set(Params.USER_OBJECT, user);
		}
		return registrationInfo;
	}

	/**
	 * Modify the exiting rentee/owner user in application.
	 * 
	 * @param registrationInfo		{@link UserInformationDTO}
	 * @return
	 * 
	 * @throws Exception
	 */
	@Override
	public UserInformationDTO modifyUserDetails(UserInformationDTO userInfo) throws Exception {

		User user = userRepository.findOne(userInfo.getId());
		if(user == null)	{
			log.info("user is not exist.");
			throw new BusinessFlowException("User is not exist.");
		}
		
		String oldPasscode = user.getPasscode();
		Date registerdDate = user.getRegistrationOfDate();
		List<AddressSpace> addressSpaces = user.getAddressSpaces();
		
		user = (User) ModelMapperUtil.map(userInfo, User.class);
		user.setPasscode(oldPasscode);
		user.setRegistrationOfDate(registerdDate);
		user.setAddressSpaces(addressSpaces);
		
		// save or merge the user.
		user = userRepository.save(user);
		
		userInfo.set(Params.USER_OBJECT, user);
		
		return userInfo;
	}
}