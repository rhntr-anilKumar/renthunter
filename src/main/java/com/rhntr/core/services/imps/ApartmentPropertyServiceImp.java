package com.rhntr.core.services.imps;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.ApartmentDTO;
import com.rhntr.core.enums.PropertyType;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.Apartment;
import com.rhntr.core.model.entities.GoogledAddress;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.ApartmentRepository;
import com.rhntr.core.model.repositories.GAddressRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.AbstractService;
import com.rhntr.core.services.ApartmentPropertyService;
import com.rhntr.utils.DateAndTimeUtil;
import com.rhntr.utils.ModelMapperUtil;

@Service
@Transactional
public class ApartmentPropertyServiceImp extends AbstractService implements ApartmentPropertyService	{

	private Logger log = LoggerFactory.getLogger(ApartmentPropertyServiceImp.class);

	@Autowired
	private GAddressRepository googleAddressRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ApartmentRepository apartmentRepo;
	
	/**
	 * @param apartmentDTO
	 * @return
	 * @throws Exception
	 */
	@Override
	public Apartment registeringNewApartmentProperty(ApartmentDTO apartmentDTO) throws Exception {

		log.debug("METHOD::registeringNewHomeProperty(HomeDTO homeDTO)");

		GoogledAddress googledAddress = googleAddressRepo.getByPlaceIdAndFormattedAddress(apartmentDTO
				.getAddressSpace().getPlaceId(), apartmentDTO.getAddressSpace().getFormattedAddress());
		
		if(googledAddress == null)	{
			googledAddress = googleAddressRepo.saveAndFlush(new ModelMapper()
					.map(apartmentDTO.getAddressSpace(), GoogledAddress.class));
			log.info("New Google address added.[" + googledAddress.getPlaceId() 
				+ " :: " + googledAddress.getFormattedAddress() + "]");
		} else {
			log.info("New property @location : " + googledAddress.getFormattedAddress());
		}

		User user = userRepo.getOne(apartmentDTO.getUser().getId());
		log.info("Property user " + user.getFirstName() + ", @id " + user.getId());

		AddressSpace addressSpace = new AddressSpace();
		addressSpace.setPropertyType(PropertyType.APARTMENT);
		addressSpace.setUser(user);
		addressSpace.setGoogledAddress(googledAddress);

		Apartment newProperty = (Apartment) ModelMapperUtil.map(apartmentDTO, Apartment.class);
		newProperty.setPostedOn(DateAndTimeUtil.currentDate());
		newProperty.setAddressSpace(addressSpace);
		
		newProperty = apartmentRepo.saveAndFlush(newProperty);
				
		log.info("apartment property id " + newProperty.getId());
		return newProperty;
	}

	@Override
	public List<ApartmentDTO> recentPostedProperty() throws Exception {
		return apartmentRepo.recentPostedProperty()
				.stream().limit(5)
				.collect(Collectors.toList());
	}
}