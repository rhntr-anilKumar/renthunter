package com.rhntr.core.services.imps;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.AbstractService;
import com.rhntr.core.services.UserAuthorisationService;

@Service(value = "userAuthService")
public class UserAuthorisationServiceImp extends AbstractService 
			implements UserAuthorisationService, UserDetailsService	{

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String userIdentity) throws UsernameNotFoundException {
		
		String emailId = null, phoneNumber = null;
		if(userIdentity.contains("@"))	{
			emailId = userIdentity;
		} else {
			phoneNumber = userIdentity;
		}
		
		User user = userRepo.findByEmailIdOrPhoneNumber(emailId, phoneNumber);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(
				user.getPhoneNumber(), user.getPasscode(), getAuthority());
	}
	
	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}
}