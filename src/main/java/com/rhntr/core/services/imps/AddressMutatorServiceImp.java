package com.rhntr.core.services.imps;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rhntr.application.dto.AddressDTO;
import com.rhntr.core.Constants;
import com.rhntr.core.Params;
import com.rhntr.core.model.entities.AddressSpace;
import com.rhntr.core.model.entities.User;
import com.rhntr.core.model.repositories.AddressRepository;
import com.rhntr.core.model.repositories.UserRepository;
import com.rhntr.core.services.AbstractService;
import com.rhntr.core.services.AddressMutatorService;
import com.rhntr.exceptions.BusinessFlowException;

/**
 * <b>AddressMutatorService	</b> <br/> <br/>
 * 
 * Address mutator service provides address related information 
 * and service for persisting any information against db.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Service Layer <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AddressMutatorService}, {@link AbstractService}
 * 
 */
@Service
@Transactional
public class AddressMutatorServiceImp extends AbstractService implements AddressMutatorService	{

	private Logger log = LoggerFactory.getLogger(AddressMutatorServiceImp.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AddressRepository addressRepository;
	
	/**
	 * Registering owner site/pg/home address.
	 * 
	 * @param addressDTO	{@link AddressDTO}
	 * @return
	 */
	@Override
	public AddressDTO addUserAddress(AddressDTO addressInfo)  throws Exception	{

		log.info("Method::addUserAddress user id : " + addressInfo.getUserId());

		User user = userRepository.findOne(addressInfo.getUserId());
		
		if(user == null)	{
			addressInfo.err("User does't exists.");
		}else	{
			
			AddressSpace addressSpace = new ModelMapper()
						.map(addressInfo, AddressSpace.class);
			log.info("inside addmut : property type : "+ addressSpace.getPropertyType());
			if(addressSpace.getGoogledAddress().getLatitude() == null)	{
				addressSpace.getGoogledAddress().setLatitude(Constants.DEFAULT_LATITUDE);
			}
			if(addressSpace.getGoogledAddress().getLongitude() == null)	{
				addressSpace.getGoogledAddress().setLongitude(Constants.DEFAULT_LONGITUDE);
			}

			addressSpace.setUser(user);
			addressSpace = addressRepository.saveAndFlush(addressSpace);
			addressInfo.set(Params.USER_ADDRESS_OBJECT, addressSpace);
		}
		return addressInfo;
	}
	
	/**
	 * Modifying existing user/owner site/pg/home address details.
	 * 
	 * @param addressDTO	{@link AddressDTO}
	 * @return
	 */
	public AddressDTO modifyUserAddressDetails(AddressDTO addressInfo) throws Exception	{
		
		log.info("Method::addUserAddress user id : " + addressInfo.getUserId());

		AddressSpace address = addressRepository.findOne(addressInfo.getId());
		
		if(address == null)	{
			addressInfo.err("This address for the user does't exists.");
		}else	{
			
			User user = userRepository.findOne(addressInfo.getUserId());
			if(user == null)	{
				throw new BusinessFlowException("User doesn't exits.");
			}

			address = new ModelMapper()
						.map(addressInfo, AddressSpace.class);
			
			if(address.getGoogledAddress().getLatitude() == null)
				address.getGoogledAddress().setLatitude(Constants.DEFAULT_LATITUDE);

			if(address.getGoogledAddress().getLongitude() == null)
				address.getGoogledAddress().setLongitude(Constants.DEFAULT_LONGITUDE);

			address.setUser(user);
			address = addressRepository.saveAndFlush(address);
			addressInfo.set(Params.USER_ADDRESS_OBJECT, address);
		}
		return addressInfo;
	}
}
