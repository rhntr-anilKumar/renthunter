package com.rhntr.core.services;

/**
 * <b>AbstractService	</b> <br/> <br/>
 * 
 * AbstractService provides common functionalities among multiple service implementation.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Access Objects <br/> <br/>
 * 
 * @author 		Anil Kumar
 * @version		1.0
 * 
 */
public abstract class AbstractService {

}
