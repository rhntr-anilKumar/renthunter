package com.rhntr.core.services;

import com.rhntr.application.dto.AddressDTO;

/**
 * <b>AddressMutatorService	</b> <br/> <br/>
 * 
 * Address mutator service provides address related information 
 * and service for persisting any information against db.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Service Layer <br/> <br/>
 * 
 * @author 		Anil Kumar
 * @version		1.0
 * 
 */
public interface AddressMutatorService {

	/**
	 * Registering owner site/pg/home address.
	 * 
	 * @param addressDTO	{@link AddressDTO}
	 * @return
	 */
	public AddressDTO addUserAddress(AddressDTO addressDTO) throws Exception;

	/**
	 * Modifying existing user/owner site/pg/home address details.
	 * 
	 * @param addressDTO	{@link AddressDTO}
	 * @return
	 */
	public AddressDTO modifyUserAddressDetails(AddressDTO addressDTO) throws Exception;
}