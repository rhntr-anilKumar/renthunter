package com.rhntr.core.services;

import com.rhntr.application.dto.PgDTO;
import com.rhntr.core.model.entities.PayingGuest;

public interface PgPropertyServices {

	/**
	 * 
	 * @param pgDTO
	 * @return
	 * @throws Exception
	 */
	public PayingGuest registeringNewPgProperty(PgDTO pgDTO)	throws Exception;
	
}
