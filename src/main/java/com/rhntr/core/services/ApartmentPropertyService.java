package com.rhntr.core.services;

import java.util.List;

import com.rhntr.application.dto.ApartmentDTO;
import com.rhntr.core.model.entities.Apartment;

public interface ApartmentPropertyService {

	/**
	 * @param homeDTO
	 * @return
	 * @throws Exception
	 */
	public Apartment registeringNewApartmentProperty(ApartmentDTO homeDTO)	throws Exception;

	/**
	 * @return
	 * @throws Exception
	 */
	public List<ApartmentDTO> recentPostedProperty() throws Exception;
}
