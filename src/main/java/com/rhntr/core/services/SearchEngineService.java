package com.rhntr.core.services;

import java.util.Map;

import com.rhntr.application.dto.AreaSearchInput;

/**
 * <b>SearchEngineService	</b> <br/> <br/>
 * 
 * Search Engine service provides search over addresses.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 02-May-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Service Layer <br/> <br/>
 * 
 * @author 		Anil Kumar
 * @version		1.0
 * 
 */
public interface SearchEngineService {

	/**
	 * Search functionality using google map generic input.
	 * 
	 * @param searchInput
	 * @return
	 */
	public Map searchAddressSpace(AreaSearchInput searchInput);
}