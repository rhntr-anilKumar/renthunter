package com.rhntr.core.services;

import java.util.List;

import com.rhntr.application.dto.HomeDTO;
import com.rhntr.core.model.entities.Home;

public interface HomePropertyService {

	/**
	 * @param homeDTO
	 * @return
	 * @throws Exception
	 */
	public Home registeringNewHomeProperty(HomeDTO homeDTO)	throws Exception;
	

	/**
	 * @return
	 * @throws Exception
	 */
	public List<HomeDTO> recentPostedProperty() throws Exception;
}
