package com.rhntr.core.services;

import com.rhntr.application.dto.UserInformationDTO;

/**
 * <b>UserMutatorService		</b> <br/> <br/>
 * 
 * User mutator service provides rentee/owener related information 
 * and service for persisting any information against db.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-March-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Service Layer <br/> <br/>
 * 
 * @author 		Anil Kumar
 * @version		1.0
 * 
 */
public interface UserMutatorService {

	/**
	 * Registering the new rentee/owner user in application.
	 * 
	 * @param registrationInfo		{@link UserInformationDTO}
	 * @return
	 * 
	 * @throws Exception
	 */
	public UserInformationDTO registerNewUser(UserInformationDTO registrationInfo) throws Exception;
	
	/**
	 * Modify the exiting rentee/owner user in application.
	 * 
	 * @param registrationInfo		{@link UserInformationDTO}
	 * @return
	 * 
	 * @throws Exception
	 */
	public UserInformationDTO modifyUserDetails(UserInformationDTO userInfo) throws Exception;
	
}