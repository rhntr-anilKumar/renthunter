package com.rhntr.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rhntr.rest.api.services.UserRegistrationController;

/**
 * <b>ResourceNotFoundException		</b> <br/> <br/>
 * 
 * Resource not found exception handler for if resource is not found.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 19-March-2018 08:10:00 AM <br/> <br/>
 * <b>Category 	: </b> Exception Handler	<br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link BusinessFlowException}, {@link GlobalExceptionHandler}
 * 
 */
public class ResourceNotFoundException extends BusinessFlowException	{
	
	private Logger log = LoggerFactory.getLogger(UserRegistrationController.class);

	private static final long serialVersionUID = 1L;
		
	/**
	 * Default constructor, 
	 * 
	 */
	public ResourceNotFoundException() {
		super();
	}
	
	/**
	 * 
	 * @param exceptionMessage	{@link String}
	 */
	public ResourceNotFoundException(final String exceptionMessage) {
		super(exceptionMessage);
	}
}