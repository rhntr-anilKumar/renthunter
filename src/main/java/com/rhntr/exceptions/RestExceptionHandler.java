package com.rhntr.exceptions;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.rhntr.utils.RestResponseUtil;


/**
 * <b>RestExceptionHandler		</b> <br/> <br/>
 * 
 * Exception handling using Spring AOP Advice.
 * Rest request body validation exception handler thrown by validation classes, 
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 21-Nov-2018 10:25:00 PM <br/> <br/>
 * <b>Category 	: </b> Exception Handler <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see 		{@link ResponseEntityExceptionHandler}, {@link MethodArgumentNotValidException}
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return RestResponseUtil.onValidationFailure(ex.getBindingResult().getAllErrors());
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, 
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String error = "Malformed JSON request : " + ex.getLocalizedMessage();
		return RestResponseUtil.badResponseEntity(error);
	}
}