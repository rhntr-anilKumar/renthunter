package com.rhntr.exceptions;

import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>ParametricException		</b> <br/> <br/>
 * 
 * Parametric exception handler for monitoring parameters 
 * and missing of required inputs that's cause in data insufficiency.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 19-March-2018 08:10:00 AM <br/> <br/>
 * <b>Category 	: </b> Exception Handler	<br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link BaseExceptionHandler}, {@link GlobalExceptionHandler}
 * 
 */
public class ParametricException extends BaseExceptionHandler	{

	private Logger log = LoggerFactory.getLogger(ParametricException.class);

	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> errorsMap = null;
	
	/**
	 * Default constructor, 
	 * 
	 */
	public ParametricException() {
		super("some inputs are missing. see errors list for details");
		this.errorsMap = new LinkedHashMap<String, Object>();
		this.errorsMap.put("inputs", "some inputs are missing.");
	}

	/**
	 * <b>ParametricException : </b> <br/> <br/>
	 * Provides the detailed way of logging exception details of missed parameter along with error message.
	 * 
	 * @param paramName				{@link String}
	 * @param exceptionMessage		{@link String}
	 */
	public ParametricException(final String paramName, final String exceptionMessage) {
		super("some inputs are missing. see errors list for details");
		this.errorsMap = new LinkedHashMap<String, Object>();
		this.errorsMap.put(paramName, exceptionMessage);
	}
	
	/**
	 * <b>ParametricException : </b> <br/> <br/>
	 * Provides the detailed way of logging exception details of all missed parameters.
	 * 
	 * @param paramName				{@link String}
	 * @param exceptionMessage		{@link String}
	 */
	public ParametricException(Map errorsMap) {
		super("some inputs are missing. see errors list for details");
		this.errorsMap = errorsMap;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.errorsMap == null ? "{}" : new JSONObject(this.errorsMap).toString();
	}
	
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return this.errorsMap == null ? "{}" : new JSONObject(this.errorsMap).toString();
	}
	
	@Override
	public String getLocalizedMessage() {
		// TODO Auto-generated method stub
		return this.errorsMap == null ? "{}" : new JSONObject(this.errorsMap).toString();
	}
}