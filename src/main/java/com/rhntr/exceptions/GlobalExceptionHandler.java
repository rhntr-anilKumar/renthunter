package com.rhntr.exceptions;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.rhntr.utils.RestResponseUtil;

/**
 * <b>GlobalExceptionHandler		</b> <br/> <br/>
 * 
 * Exception handling using Spring AOP Advice and forming rest error response entity 
 * for all type of exception that raise in application.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 16-March-2018 11:42:00 AM <br/> <br/>
 * <b>Category 	: </b> Exception Handler <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see 		{@link BaseExceptionHandler}, {@link BusinessFlowException}, {@link ParametricException}
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	/**
	 * <b>ResourceNotFoundException</b> is thrown whenever any resource/objects not found.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link ResourceNotFoundException}
	 * @return
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity handleResourceNotFoundException(HttpServletRequest request, ResourceNotFoundException exception)	{
		log.info("@ResourceNotFoundException : " + exception.getMessage());
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}

	/**
	 * <b>ParametricException</b> is thrown when some input are missed in request scope.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link ParametricException}
	 * @return
	 */
	@ExceptionHandler(ParametricException.class)
	public ResponseEntity handleParametricException(HttpServletRequest request, ParametricException exception)	{
		log.info("@ParametricException : " + exception.getMessage());
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}
	
	/**
	 * <b>BusinessFlowException</b> is thrown when business flow is missing its requirements.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link BusinessFlowException}
	 * @return
	 */
	@ExceptionHandler(BusinessFlowException.class)
	public ResponseEntity handleBusinessFlowException(HttpServletRequest request, BusinessFlowException exception)	{
		log.info("@BusinessFlowException : " + exception.getMessage());
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}

	/**
	 * <b>JDBCException</b> is thrown when there is problem with JDBC connection/execution.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link JDBCException}
	 * @return
	 */
	@ExceptionHandler(JDBCException.class)
	public ResponseEntity handleJDBCException(HttpServletRequest request, JDBCException exception)	{
		log.info("@JDBCException : [" + exception.getErrorCode() + "] " + exception.getMessage());
		log.info("@JDBCException state : " + exception.getSQLState() + " -> " + exception.getSQL());
		exception.printStackTrace();
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}
	
	/**
	 * <b>JDBCException</b> is thrown when there is problem with JDBC connection/execution.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link SQLException}
	 * @return
	 */
	@ExceptionHandler(SQLException.class)
	public ResponseEntity handleSQLException(HttpServletRequest request, SQLException exception)	{
		log.info("@SQLException : [" + exception.getErrorCode() + " : " + exception.getSQLState() + "] " + exception.getMessage());
		exception.printStackTrace();
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}
	
	/**
	 * <b>HibernateException</b> is thrown when hibernate queries get into any errors..
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link HibernateException}
	 * @return
	 */
	@ExceptionHandler(HibernateException.class)
	public ResponseEntity handleHibernateException(HttpServletRequest request, HibernateException exception)	{
		log.info("@HibernateException : " + exception.getMessage());
		exception.printStackTrace();
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}
	
	/**
	 * <b>Exception</b> is caught if generic exception is thrown from application.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link Exception}
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity handleException(HttpServletRequest request, Exception exception)	{
		log.info("@Exception : " + exception.getMessage());
		exception.printStackTrace();
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}

	/**
	 * <b>RuntimeException</b> is caught if any runtime exception is thrown from application.
	 * 
	 * @param request		{@link HttpServletRequest}
	 * @param exception		{@link Exception}
	 * @return
	 */
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity handleRuntimeException(HttpServletRequest request, RuntimeException exception)	{
		log.info("@RuntimeException : " + exception.getMessage());
		exception.printStackTrace();
		return RestResponseUtil.badResponseEntity(exception.getMessage());
	}	
}