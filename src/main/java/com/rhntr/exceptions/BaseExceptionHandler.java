package com.rhntr.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>BaseExceptionHandler		</b> <br/> <br/>
 * 
 * Base exception handler provides common logging and 
 * internationalization (i18n) message conversions if required...
 * and common functionalities while handling exceptions.
 * 
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 19-March-2018 08:10:00 AM <br/> <br/>
 * <b>Category 	: </b> Exception Handler	<br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link Exception}, {@link RuntimeException}, {@link GlobalExceptionHandler}
 */
public class BaseExceptionHandler extends RuntimeException	{

	private Logger log = LoggerFactory.getLogger(BaseExceptionHandler.class);

	private static final long serialVersionUID = 1L;
	
	/**
	 * default constructor,
	 */
	public BaseExceptionHandler() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	/**
	 * 
	 * @param exceptionMessage		{@link String}
	 */
	public BaseExceptionHandler(final String exceptionMessage) {
		// TODO Auto-generated constructor stub
		super(exceptionMessage);
	}
}