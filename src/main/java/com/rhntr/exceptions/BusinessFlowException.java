package com.rhntr.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>BusinessFlowException		</b> <br/> <br/>
 * 
 * Business flow exception handler for flow control, 
 * which mainly focus on violation of business flow against requirement
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 19-March-2018 08:10:00 AM <br/> <br/>
 * <b>Category 	: </b> Exception Handler	<br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link BaseExceptionHandler}, {@link GlobalExceptionHandler}
 * 
 */
public class BusinessFlowException extends BaseExceptionHandler	{

	private Logger log = LoggerFactory.getLogger(BusinessFlowException.class);

	private static final long serialVersionUID = 1L;

	/**
	 * default constructor,
	 */
	public BusinessFlowException() {
		super();
	}
	
	/**
	 * <b>BusinessFlowException : </b> <br/> <br/>
	 * Business oriented message logging exception handler for 
	 * violation of business flows.
	 * 
	 * @param exceptionMessage		{@link String}
	 */
	public BusinessFlowException(final String exceptionMessage) {
		super(exceptionMessage);
	}
}