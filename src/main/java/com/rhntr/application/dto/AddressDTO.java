package com.rhntr.application.dto;

import com.rhntr.core.enums.PropertyType;

/**
 * <b>AddressDTO	</b> <br/> <br/>
 * 
 * AddressDTO provides address mapping details and user information.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-Jan-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Transfer Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AbstractDTO}
 * 
 */
public class AddressDTO extends AbstractDTO	{
	

	private Float latitude;
	
	private Float longitude;
	
    private String placeId;

    private String formattedAddress;

    private String streetNumber;

    private String street;

	private String landMark;

	private String route;

    private String sublocalityLevel2;

    private String sublocalityLevel1;

    private String locality;

    private String administrativeAreaLevel2;

    private String administrativeAreaLevel1;

    private String country;

    private String postalCode;

	private Long userId;
	
	private PropertyType propertyType;

	/**
	 * @return the latitude
	 */
	public Float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the placeId
	 */
	public String getPlaceId() {
		return placeId;
	}

	/**
	 * @param placeId the placeId to set
	 */
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	/**
	 * @return the formattedAddress
	 */
	public String getFormattedAddress() {
		return formattedAddress;
	}

	/**
	 * @param formattedAddress the formattedAddress to set
	 */
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}

	/**
	 * @return the streetNumber
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @param streetNumber the streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the route
	 */
	public String getRoute() {
		return route;
	}

	/**
	 * @param route the route to set
	 */
	public void setRoute(String route) {
		this.route = route;
	}

	/**
	 * @return the sublocalityLevel2
	 */
	public String getSublocalityLevel2() {
		return sublocalityLevel2;
	}

	/**
	 * @param sublocalityLevel2 the sublocalityLevel2 to set
	 */
	public void setSublocalityLevel2(String sublocalityLevel2) {
		this.sublocalityLevel2 = sublocalityLevel2;
	}

	/**
	 * @return the sublocalityLevel1
	 */
	public String getSublocalityLevel1() {
		return sublocalityLevel1;
	}

	/**
	 * @param sublocalityLevel1 the sublocalityLevel1 to set
	 */
	public void setSublocalityLevel1(String sublocalityLevel1) {
		this.sublocalityLevel1 = sublocalityLevel1;
	}

	/**
	 * @return the locality
	 */
	public String getLocality() {
		return locality;
	}

	/**
	 * @param locality the locality to set
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}

	/**
	 * @return the administrativeAreaLevel2
	 */
	public String getAdministrativeAreaLevel2() {
		return administrativeAreaLevel2;
	}

	/**
	 * @param administrativeAreaLevel2 the administrativeAreaLevel2 to set
	 */
	public void setAdministrativeAreaLevel2(String administrativeAreaLevel2) {
		this.administrativeAreaLevel2 = administrativeAreaLevel2;
	}

	/**
	 * @return the administrativeAreaLevel1
	 */
	public String getAdministrativeAreaLevel1() {
		return administrativeAreaLevel1;
	}

	/**
	 * @param administrativeAreaLevel1 the administrativeAreaLevel1 to set
	 */
	public void setAdministrativeAreaLevel1(String administrativeAreaLevel1) {
		this.administrativeAreaLevel1 = administrativeAreaLevel1;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the propertyType
	 */
	public PropertyType getPropertyType() {
		return propertyType;
	}

	/**
	 * @param propertyType the propertyType to set
	 */
	public void setPropertyType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * @return the landMark
	 */
	public String getLandMark() {
		return landMark;
	}

	/**
	 * @param landMark the landMark to set
	 */
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
}