package com.rhntr.application.dto;

import java.util.Date;
import java.util.List;

import com.rhntr.core.enums.Availability;
import com.rhntr.core.enums.Furnished;
import com.rhntr.core.enums.RentHomeSpace;
import com.rhntr.core.enums.TenantsType;
import com.rhntr.core.model.entities.HomePictures;
import com.rhntr.core.model.entities.User;

/**
 * <b>HomeDTO	</b> <br/> <br/>
 * HomeDTO provides Home details.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 03-May-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Transfer Objects <br/> <br/>
 * @version		1.0
 * @author 		Maddy
 * @see			{@link AbstractDTO}
 *
 */
public class HomeDTO extends AbstractDTO{
	
	private String name;
	private Date constructedOn;
	private TenantsType tenant;
	private RentHomeSpace bhkLength;
	private short floor;
	private Furnished furnished;

	private Boolean nonVegAllowed;
	private Boolean petsAllowed;
	private Boolean parking;
	
	
	private Long rent;
	private Long deposit;
	private Availability availability;
	private Long maintainanceCharge;
	private Long agreemantDuriation;
	private Integer buildUpArea;
	private Boolean negotiable;
	private Boolean depositRefundable;
	private short leavingIntimation;

	private Integer availableRooms;
	private Integer totalRooms;
	
	private String formattedAddress;
	private AddressDTO addressSpace;
	
	private User user;
	
	private List<HomePictures> homePictures;

	
	
	public HomeDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor for loading limited information of property
	 * 
	 * @param id
	 * @param rent
	 * @param deposit
	 * @param bhkLength
	 * @param buildUpArea
	 * @param formattedAddress
	 */
	public HomeDTO(Long id, Long rent, Long deposit, RentHomeSpace bhkLength, 
			Integer buildUpArea, String formattedAddress)	{
		this.id = id;
		this.rent = rent;
		this.deposit = deposit;
		this.bhkLength = bhkLength;
		this.buildUpArea = buildUpArea;
		this.formattedAddress = formattedAddress;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the constructedOn
	 */
	public Date getConstructedOn() {
		return constructedOn;
	}

	/**
	 * @param constructedOn the constructedOn to set
	 */
	public void setConstructedOn(Date constructedOn) {
		this.constructedOn = constructedOn;
	}

	/**
	 * @return the tenant
	 */
	public TenantsType getTenant() {
		return tenant;
	}

	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(TenantsType tenant) {
		this.tenant = tenant;
	}

	/**
	 * @return the buildUpArea
	 */
	public Integer getBuildUpArea() {
		return buildUpArea;
	}

	/**
	 * @param buildUpArea the buildUpArea to set
	 */
	public void setBuildUpArea(Integer buildUpArea) {
		this.buildUpArea = buildUpArea;
	}

	/**
	 * @return the nonVegAllowed
	 */
	public Boolean getNonVegAllowed() {
		return nonVegAllowed;
	}

	/**
	 * @param nonVegAllowed the nonVegAllowed to set
	 */
	public void setNonVegAllowed(Boolean nonVegAllowed) {
		this.nonVegAllowed = nonVegAllowed;
	}

	/**
	 * @return the petsAllowed
	 */
	public Boolean getPetsAllowed() {
		return petsAllowed;
	}

	/**
	 * @param petsAllowed the petsAllowed to set
	 */
	public void setPetsAllowed(Boolean petsAllowed) {
		this.petsAllowed = petsAllowed;
	}

	/**
	 * @return the parking
	 */
	public Boolean getParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(Boolean parking) {
		this.parking = parking;
	}

	/**
	 * @return the negotiable
	 */
	public Boolean getNegotiable() {
		return negotiable;
	}

	/**
	 * @param negotiable the negotiable to set
	 */
	public void setNegotiable(Boolean negotiable) {
		this.negotiable = negotiable;
	}

	/**
	 * @return the depositRefundable
	 */
	public Boolean getDepositRefundable() {
		return depositRefundable;
	}

	/**
	 * @param depositRefundable the depositRefundable to set
	 */
	public void setDepositRefundable(Boolean depositRefundable) {
		this.depositRefundable = depositRefundable;
	}

	/**
	 * @return the maintainanceCharge
	 */
	public Long getMaintainanceCharge() {
		return maintainanceCharge;
	}

	/**
	 * @param maintainanceCharge the maintainanceCharge to set
	 */
	public void setMaintainanceCharge(Long maintainanceCharge) {
		this.maintainanceCharge = maintainanceCharge;
	}

	/**
	 * @return the agreemantDuriation
	 */
	public Long getAgreemantDuriation() {
		return agreemantDuriation;
	}

	/**
	 * @param agreemantDuriation the agreemantDuriation to set
	 */
	public void setAgreemantDuriation(Long agreemantDuriation) {
		this.agreemantDuriation = agreemantDuriation;
	}

	/**
	 * @return the leavingIntimation
	 */
	public short getLeavingIntimation() {
		return leavingIntimation;
	}

	/**
	 * @param leavingIntimation the leavingIntimation to set
	 */
	public void setLeavingIntimation(short leavingIntimation) {
		this.leavingIntimation = leavingIntimation;
	}

	/**
	 * @return the availableRooms
	 */
	public Integer getAvailableRooms() {
		return availableRooms;
	}

	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(Integer availableRooms) {
		this.availableRooms = availableRooms;
	}

	/**
	 * @return the totalRooms
	 */
	public Integer getTotalRooms() {
		return totalRooms;
	}

	/**
	 * @param totalRooms the totalRooms to set
	 */
	public void setTotalRooms(Integer totalRooms) {
		this.totalRooms = totalRooms;
	}

	/**
	 * @return the addressSpace
	 */
	public AddressDTO getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the address to set
	 */
	public void setAddressSpace(AddressDTO addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the bhkLength
	 */
	public RentHomeSpace getBhkLength() {
		return bhkLength;
	}

	/**
	 * @param bhkLength the bhkLength to set
	 */
	public void setBhkLength(RentHomeSpace bhkLength) {
		this.bhkLength = bhkLength;
	}

	/**
	 * @return the floor
	 */
	public short getFloor() {
		return floor;
	}

	/**
	 * @param floor the floor to set
	 */
	public void setFloor(short floor) {
		this.floor = floor;
	}

	/**
	 * @return the furnished
	 */
	public Furnished getFurnished() {
		return furnished;
	}

	/**
	 * @param furnished the furnished to set
	 */
	public void setFurnished(Furnished furnished) {
		this.furnished = furnished;
	}

	/**
	 * @return the deposit
	 */
	public Long getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(Long deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the rent
	 */
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the availability
	 */
	public Availability getAvailability() {
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	/**
	 * @return the homePictures
	 */
	public List<HomePictures> getHomePictures() {
		return homePictures;
	}

	/**
	 * @param homePictures the homePictures to set
	 */
	public void setHomePictures(List<HomePictures> homePictures) {
		this.homePictures = homePictures;
	}

	/**
	 * @return the formattedAddress
	 */
	public String getFormattedAddress() {
		return formattedAddress;
	}

	/**
	 * @param formattedAddress the formattedAddress to set
	 */
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}
}