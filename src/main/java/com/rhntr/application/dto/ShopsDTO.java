package com.rhntr.application.dto;

/**
 * <b>ShopsDTO		</b> <br/> <br/>
 * 
 * Shops DTO provides Shop Details
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 03-Mar-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Madhu
 * 
 */

public class ShopsDTO  extends AbstractDTO{
	
	private Long id;

	private Long squareFeet;
	
	private Long width;
	
	private Long length;
	
	private Long deposit;
	
	private Long rent;
	
	private Long adresssSpaceId;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the squareFeet
	 */
	public Long getSquareFeet() {
		return squareFeet;
	}

	/**
	 * @param squareFeet the squareFeet to set
	 */
	public void setSquareFeet(Long squareFeet) {
		this.squareFeet = squareFeet;
	}

	/**
	 * @return the width
	 */
	public Long getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(Long width) {
		this.width = width;
	}

	/**
	 * @return the length
	 */
	public Long getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(Long length) {
		this.length = length;
	}

	/**
	 * @return the deposit
	 */
	public Long getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(Long deposit) {
		this.deposit = deposit;
	}

	/**
	 * @return the rent
	 */
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the adresssSpaceId
	 */
	public Long getAdresssSpaceId() {
		return adresssSpaceId;
	}

	/**
	 * @param adresssSpaceId the adresssSpaceId to set
	 */
	public void setAdresssSpaceId(Long adresssSpaceId) {
		this.adresssSpaceId = adresssSpaceId;
	}

	
}
