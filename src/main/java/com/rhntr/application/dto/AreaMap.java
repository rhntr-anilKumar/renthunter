package com.rhntr.application.dto;

import java.util.Map;

/**
 * <b>AreaMap	</b> <br/> <br/>
 * 
 * AreaMap provides current location/area information.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-Jan-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Transfer Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AbstractDTO}
 * 
 */
public class AreaMap extends AbstractDTO	{

	
}