package com.rhntr.application.dto;

import java.util.Date;
import java.util.List;

import com.rhntr.core.enums.Furnished;
import com.rhntr.core.enums.ParkingSpace;
import com.rhntr.core.enums.RentHomeSpace;
import com.rhntr.core.enums.TenantsType;
import com.rhntr.core.model.entities.ApartmentPictures;
import com.rhntr.core.model.entities.User;

/**
 * <b>ApartmentDTO		</b> <br/> <br/>
 * 
 * Apartment DTO provides Home Details
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 03-Mar-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Entity Class <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * 
 */
public class ApartmentDTO extends AbstractDTO	{

	private String name;
	private Date constructedOn;
	private TenantsType tenant;
	private Integer buildUpArea;
	private RentHomeSpace bhkLength;
	private Furnished furnished;

	private Boolean nonVegAllowed;
	private Boolean petsAllowed;
	private ParkingSpace parking;
	private Boolean gardenCityAvailable;
	private Boolean swimmingPoolAvailable;
	private Boolean underCCTVServilence;
	private Boolean securityGuardianAreExist;
	private Boolean functionHallExist;

	private Long rent;
	private Long deposite;
	private Boolean negotiable;
	private Boolean depositRefundable;	
	private Integer maintainanceCharge;
	private Integer agreemantDuriation;	
	private short leavingIntimationBefore;

	private User user;
	
	private String formattedAddress;
	private AddressDTO addressSpace;
	private List<ApartmentPictures> apartmentPictures;

	/**
	 * Constructor for loading limited information of property
	 * 
	 * @param id
	 * @param rent
	 * @param deposit
	 * @param bhkLength
	 * @param buildUpArea
	 * @param formattedAddress
	 */
	public ApartmentDTO(Long id, Long rent, Long deposit, RentHomeSpace bhkLength, 
			Integer buildUpArea, String formattedAddress)	{
		this.id = id;
		this.rent = rent;
		this.deposite = deposit;
		this.bhkLength = bhkLength;
		this.buildUpArea = buildUpArea;
		this.formattedAddress = formattedAddress;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the constructedOn
	 */
	public Date getConstructedOn() {
		return constructedOn;
	}

	/**
	 * @param constructedOn the constructedOn to set
	 */
	public void setConstructedOn(Date constructedOn) {
		this.constructedOn = constructedOn;
	}

	/**
	 * @return the tenant
	 */
	public TenantsType getTenant() {
		return tenant;
	}

	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(TenantsType tenant) {
		this.tenant = tenant;
	}

	/**
	 * @return the buildUpArea
	 */
	public Integer getBuildUpArea() {
		return buildUpArea;
	}

	/**
	 * @param buildUpArea the buildUpArea to set
	 */
	public void setBuildUpArea(Integer buildUpArea) {
		this.buildUpArea = buildUpArea;
	}

	/**
	 * @return the furnished
	 */
	public Furnished getFurnished() {
		return furnished;
	}

	/**
	 * @param furnished the furnished to set
	 */
	public void setFurnished(Furnished furnished) {
		this.furnished = furnished;
	}

	/**
	 * @return the nonVegAllowed
	 */
	public Boolean getNonVegAllowed() {
		return nonVegAllowed;
	}

	/**
	 * @param nonVegAllowed the nonVegAllowed to set
	 */
	public void setNonVegAllowed(Boolean nonVegAllowed) {
		this.nonVegAllowed = nonVegAllowed;
	}

	/**
	 * @return the petsAllowed
	 */
	public Boolean getPetsAllowed() {
		return petsAllowed;
	}

	/**
	 * @param petsAllowed the petsAllowed to set
	 */
	public void setPetsAllowed(Boolean petsAllowed) {
		this.petsAllowed = petsAllowed;
	}

	/**
	 * @return the parking
	 */
	public ParkingSpace getParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(ParkingSpace parking) {
		this.parking = parking;
	}

	/**
	 * @return the gardenCityAvailable
	 */
	public Boolean getGardenCityAvailable() {
		return gardenCityAvailable;
	}

	/**
	 * @param gardenCityAvailable the gardenCityAvailable to set
	 */
	public void setGardenCityAvailable(Boolean gardenCityAvailable) {
		this.gardenCityAvailable = gardenCityAvailable;
	}

	/**
	 * @return the swimmingPoolAvailable
	 */
	public Boolean getSwimmingPoolAvailable() {
		return swimmingPoolAvailable;
	}

	/**
	 * @param swimmingPoolAvailable the swimmingPoolAvailable to set
	 */
	public void setSwimmingPoolAvailable(Boolean swimmingPoolAvailable) {
		this.swimmingPoolAvailable = swimmingPoolAvailable;
	}

	/**
	 * @return the underCCTVServilence
	 */
	public Boolean getUnderCCTVServilence() {
		return underCCTVServilence;
	}

	/**
	 * @param underCCTVServilence the underCCTVServilence to set
	 */
	public void setUnderCCTVServilence(Boolean underCCTVServilence) {
		this.underCCTVServilence = underCCTVServilence;
	}

	/**
	 * @return the securityGuardianAreExist
	 */
	public Boolean getSecurityGuardianAreExist() {
		return securityGuardianAreExist;
	}

	/**
	 * @param securityGuardianAreExist the securityGuardianAreExist to set
	 */
	public void setSecurityGuardianAreExist(Boolean securityGuardianAreExist) {
		this.securityGuardianAreExist = securityGuardianAreExist;
	}

	/**
	 * @return the functionHallExist
	 */
	public Boolean getFunctionHallExist() {
		return functionHallExist;
	}

	/**
	 * @param functionHallExist the functionHallExist to set
	 */
	public void setFunctionHallExist(Boolean functionHallExist) {
		this.functionHallExist = functionHallExist;
	}

	/**
	 * @return the rent
	 */
	public Long getRent() {
		return rent;
	}

	/**
	 * @param rent the rent to set
	 */
	public void setRent(Long rent) {
		this.rent = rent;
	}

	/**
	 * @return the deposite
	 */
	public Long getDeposite() {
		return deposite;
	}

	/**
	 * @param deposite the deposite to set
	 */
	public void setDeposite(Long deposite) {
		this.deposite = deposite;
	}

	/**
	 * @return the negotiable
	 */
	public Boolean getNegotiable() {
		return negotiable;
	}

	/**
	 * @param negotiable the negotiable to set
	 */
	public void setNegotiable(Boolean negotiable) {
		this.negotiable = negotiable;
	}

	/**
	 * @return the depositRefundable
	 */
	public Boolean getDepositRefundable() {
		return depositRefundable;
	}

	/**
	 * @param depositRefundable the depositRefundable to set
	 */
	public void setDepositRefundable(Boolean depositRefundable) {
		this.depositRefundable = depositRefundable;
	}

	/**
	 * @return the maintainanceCharge
	 */
	public Integer getMaintainanceCharge() {
		return maintainanceCharge;
	}

	/**
	 * @param maintainanceCharge the maintainanceCharge to set
	 */
	public void setMaintainanceCharge(Integer maintainanceCharge) {
		this.maintainanceCharge = maintainanceCharge;
	}

	/**
	 * @return the agreemantDuriation
	 */
	public Integer getAgreemantDuriation() {
		return agreemantDuriation;
	}

	/**
	 * @param agreemantDuriation the agreemantDuriation to set
	 */
	public void setAgreemantDuriation(Integer agreemantDuriation) {
		this.agreemantDuriation = agreemantDuriation;
	}

	/**
	 * @return the leavingIntimationBefore
	 */
	public short getLeavingIntimationBefore() {
		return leavingIntimationBefore;
	}

	/**
	 * @param leavingIntimationBefore the leavingIntimationBefore to set
	 */
	public void setLeavingIntimationBefore(short leavingIntimationBefore) {
		this.leavingIntimationBefore = leavingIntimationBefore;
	}

	/**
	 * @return the addressSpace
	 */
	public AddressDTO getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(AddressDTO addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the apartmentPictures
	 */
	public List<ApartmentPictures> getApartmentPictures() {
		return apartmentPictures;
	}

	/**
	 * @param apartmentPictures the apartmentPictures to set
	 */
	public void setApartmentPictures(List<ApartmentPictures> apartmentPictures) {
		this.apartmentPictures = apartmentPictures;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the bhkLength
	 */
	public RentHomeSpace getBhkLength() {
		return bhkLength;
	}

	/**
	 * @param bhkLength the bhkLength to set
	 */
	public void setBhkLength(RentHomeSpace bhkLength) {
		this.bhkLength = bhkLength;
	}

	/**
	 * @return the formattedAddress
	 */
	public String getFormattedAddress() {
		return formattedAddress;
	}

	/**
	 * @param formattedAddress the formattedAddress to set
	 */
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}
}