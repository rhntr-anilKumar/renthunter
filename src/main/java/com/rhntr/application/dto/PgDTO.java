package com.rhntr.application.dto;

import java.util.List;

import com.rhntr.core.enums.FoodType;
import com.rhntr.core.enums.PGType;
import com.rhntr.core.model.entities.FilesList;
import com.rhntr.core.model.entities.PGPictures;
import com.rhntr.core.model.entities.PGSpaceProperties;
import com.rhntr.core.model.entities.User;

/**
 * <b>PgDTO	</b> <br/> <br/>
 * 
 * Pg dto provides information and service of pg property.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 04-May-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Transfer Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AbstractDTO}
 * 
 */
public class PgDTO extends AbstractDTO	{

	private PGType type;

	private String name;
	
	private FoodType foodType;
	
	private Integer availableRooms;
	
	private Integer totalRooms;

	private String advance;
	
	private Boolean parking;
	
	private Boolean refundable;
	
	private Boolean negotiable;
	
	private Boolean underCCTVServilence;

	private Boolean securityGuardianAreExist;
	
	private List<PGSpaceProperties> spaceProperties;

	private AddressDTO addressSpace;
	
	private User user; 
	
	private List<PGPictures> pgPictures;

	/**
	 * @return the type
	 */
	public PGType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(PGType type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the foodType
	 */
	public FoodType getFoodType() {
		return foodType;
	}

	/**
	 * @param foodType the foodType to set
	 */
	public void setFoodType(FoodType foodType) {
		this.foodType = foodType;
	}

	/**
	 * @return the availableRooms
	 */
	public Integer getAvailableRooms() {
		return availableRooms;
	}

	/**
	 * @param availableRooms the availableRooms to set
	 */
	public void setAvailableRooms(Integer availableRooms) {
		this.availableRooms = availableRooms;
	}

	/**
	 * @return the totalRooms
	 */
	public Integer getTotalRooms() {
		return totalRooms;
	}

	/**
	 * @param totalRooms the totalRooms to set
	 */
	public void setTotalRooms(Integer totalRooms) {
		this.totalRooms = totalRooms;
	}

	/**
	 * @return the advance
	 */
	public String getAdvance() {
		return advance;
	}

	/**
	 * @param advance the advance to set
	 */
	public void setAdvance(String advance) {
		this.advance = advance;
	}

	/**
	 * @return the underCCTVServilence
	 */
	public Boolean getUnderCCTVServilence() {
		return underCCTVServilence;
	}

	/**
	 * @param underCCTVServilence the underCCTVServilence to set
	 */
	public void setUnderCCTVServilence(Boolean underCCTVServilence) {
		this.underCCTVServilence = underCCTVServilence;
	}

	/**
	 * @return the securityGuardianAreExist
	 */
	public Boolean getSecurityGuardianAreExist() {
		return securityGuardianAreExist;
	}

	/**
	 * @param securityGuardianAreExist the securityGuardianAreExist to set
	 */
	public void setSecurityGuardianAreExist(Boolean securityGuardianAreExist) {
		this.securityGuardianAreExist = securityGuardianAreExist;
	}

	/**
	 * @return the spaceProperties
	 */
	public List<PGSpaceProperties> getSpaceProperties() {
		return spaceProperties;
	}

	/**
	 * @param spaceProperties the spaceProperties to set
	 */
	public void setSpaceProperties(List<PGSpaceProperties> spaceProperties) {
		this.spaceProperties = spaceProperties;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the parking
	 */
	public Boolean getParking() {
		return parking;
	}

	/**
	 * @param parking the parking to set
	 */
	public void setParking(Boolean parking) {
		this.parking = parking;
	}

	/**
	 * @return the refundable
	 */
	public Boolean getRefundable() {
		return refundable;
	}

	/**
	 * @param refundable the refundable to set
	 */
	public void setRefundable(Boolean refundable) {
		this.refundable = refundable;
	}

	/**
	 * @return the negotiable
	 */
	public Boolean getNegotiable() {
		return negotiable;
	}

	/**
	 * @param negotiable the negotiable to set
	 */
	public void setNegotiable(Boolean negotiable) {
		this.negotiable = negotiable;
	}

	/**
	 * @return the addressSpace
	 */
	public AddressDTO getAddressSpace() {
		return addressSpace;
	}

	/**
	 * @param addressSpace the addressSpace to set
	 */
	public void setAddressSpace(AddressDTO addressSpace) {
		this.addressSpace = addressSpace;
	}

	/**
	 * @return the pgPictures
	 */
	public List<PGPictures> getPgPictures() {
		return pgPictures;
	}

	/**
	 * @param pgPictures the pgPictures to set
	 */
	public void setPgPictures(List<PGPictures> pgPictures) {
		this.pgPictures = pgPictures;
	}
}