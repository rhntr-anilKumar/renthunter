package com.rhntr.application.dto;

import java.util.Date;

/**
 * <b>UserRegistrationDTO	</b> <br/> <br/>
 * 
 * User registration dto provides user information registration of new user(rentee/owner).
 * Also provides the rest base form validation on inputs using javax validation framework.
 * 
 * <br/> <br/> <br/>
 * 
 * <b>Date 		: </b> 15-Jan-2018 12:00:00 AM <br/> <br/>
 * <b>Category 	: </b> Data Transfer Objects <br/> <br/>
 * 
 * @version		1.0
 * @author 		Anil Kumar
 * @see			{@link AbstractDTO}
 * 
 */
public class UserInformationDTO extends AbstractDTO	{

	private String firstName;

	private String lastName;

	private Date dateOfBirth;
	
	private String emailId;
	
	private String phoneNumber;

	private String passcode;

	private String confirmPasscode;

	public UserInformationDTO() {

	}

	/**
	 * @return the passcode
	 */
	public String getPasscode() {
		return passcode;
	}

	/**
	 * @param passcode the passcode to set
	 */
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	/**
	 * @return the confirmPasscode
	 */
	public String getConfirmPasscode() {
		return confirmPasscode;
	}

	/**
	 * @param confirmPasscode the confirmPasscode to set
	 */
	public void setConfirmPasscode(String confirmPasscode) {
		this.confirmPasscode = confirmPasscode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}