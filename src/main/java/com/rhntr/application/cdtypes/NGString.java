package com.rhntr.application.cdtypes;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;


public final class NGString implements java.io.Serializable, Comparable<NGString> {

	private static final long serialVersionUID = -5325191385934339161L;

	private static Map<String, Boolean> possibleBooleanValues = new HashMap<String, Boolean>(10);
	
	static {
		possibleBooleanValues.put("t", true);
		possibleBooleanValues.put("f", false);
		possibleBooleanValues.put("1", true);
		possibleBooleanValues.put("0", false);
		possibleBooleanValues.put("true", true);
		possibleBooleanValues.put("false", false);
		possibleBooleanValues.put("yes", true);
		possibleBooleanValues.put("no", false);
		possibleBooleanValues.put("on", true);
		possibleBooleanValues.put("off", false);
	}
	
	private String string;

	private static final String EMPTY_STRING = new String();

	public NGString() {
		string = new String();
	}

	public NGString(String str)	{
		this.string = str;
	}

	public NGString(char[] str)	{
		this.string = new String(str);
	}

	public boolean isEmpty() {
		return this.string.isEmpty();
	}

	public NGString encode() {
		this.string = URLEncoder.encode(this.string);
		return this;
	}

	public NGString getEncoded() {
		return new NGString(URLEncoder.encode(this.string));
	}

	public NGString encode(String enc) throws UnsupportedEncodingException{
		this.string = URLEncoder.encode(this.string, enc);
		return this;
	}

	public NGString getEncoded(String enc) throws UnsupportedEncodingException{
		return new NGString(URLEncoder.encode(this.string, enc));
	}

	public NGString decode() {
		this.string = URLDecoder.decode(this.string);
		return this;
	}

	public NGString getDecoded() {
		return new NGString(URLDecoder.decode(this.string));
	}

	public NGString decode(String enc) throws UnsupportedEncodingException {
		this.string = URLDecoder.decode(this.string, enc);
		return this;
	}

	public NGString getDecoded(String enc) throws UnsupportedEncodingException {
		this.string = URLDecoder.decode(this.string, enc);
		return this;
	}

	public static boolean isNull(String str)	{
		return str == null;
	}

	public static boolean isNull(NGString ngString)	{
		return ngString == null;
	}

	public static boolean isNullOrEmpty(String str)	{
		return str == null || EMPTY_STRING.equals(str.trim());
	}

	public static boolean isNullOrEmpty(NGString ngString)	{
		return ngString == null || EMPTY_STRING.equals(ngString.string.trim());
	}

	public int indexOf(int ch) {
		return this.string.indexOf(ch);
	}

	public int indexOf(String str) {
		return this.string.indexOf(str);
	}

	public int indexOf(NGString ngString) {
		return this.string.indexOf(ngString.string);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return string;
	}

	public byte[] getBytes() {
		return this.string.getBytes();
	}

	public byte[] getBytes(Charset charset) {
		return this.string.getBytes(charset);
	}

	public byte[] getBytes(String charsetName) throws UnsupportedEncodingException {
		return this.string.getBytes(charsetName);
	}

	public void getBytes(int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		this.string.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}

	public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin) {
		this.string.getChars(srcBegin, srcEnd, dst, dstBegin);
	}

	public IntStream chars() {
		return this.string.chars();
	}

	public NGString concat(String str) {
		this.string.concat(str);
		return this;
	}

	public NGString concat(NGString ngString) {
		this.string.concat(ngString.string);
		return this;
	}

	public boolean startsWith(String prefix) {
		return this.string.startsWith(prefix);
	}

	public boolean startsWith(NGString prefix) {
		return this.string.startsWith(prefix.string);
	}

	public boolean startsWith(String prefix, int toffset) {
		return this.string.startsWith(prefix, toffset);
	}

	public boolean startsWith(NGString prefix, int toffset) {
		return this.string.startsWith(prefix.string, toffset);
	}

	public boolean endsWith(String suffix) {
		return this.string.endsWith(suffix);
	}

	public boolean endsWith(NGString suffix) {
		return this.string.endsWith(suffix.string);
	}

	public boolean contains(CharSequence charSequence) {
		return this.string.contains(charSequence);
	}

	public boolean contains(NGString ngString) {
		return this.string.contains(ngString.string);
	}

	@Override
	public int hashCode() {
		return this.string.hashCode();
	}

	@Override
	public boolean equals(Object anObject) {
		return this.string.equals(anObject);
	}

	public boolean equals(NGString ngString) {
		return this.string.equals(ngString.string);
	}

	public boolean equalsIgnoreCase(String anotherString) {
		return this.string.equalsIgnoreCase(anotherString);
	}

	public boolean equalsIgnoreCase(NGString anotherString) {
		return this.string.equalsIgnoreCase(anotherString.string);
	}

	public NGString append(String str) {
		this.string += str;
		return this;
	}

	public NGString append(NGString ngString) {
		this.string += ngString.string;
		return this;
	}

	public NGString append(Object obj) {
		this.string += String.valueOf(obj);
		return this;
	}

	public int compareTo(String anotherString) {
		return this.string.compareTo(anotherString);
	}

	@Override
	public int compareTo(NGString anotherString) {
		return this.string.compareTo(anotherString.string);
	}

	public int compareToIgnoreCase(String anotherString) {
		return this.string.compareToIgnoreCase(anotherString);
	}

	public int compareToIgnoreCase(NGString anotherString) {
		return this.string.compareToIgnoreCase(anotherString.string);
	}
	
	public Integer toInteger() {
		try {
			return new Integer(this.string);
		}catch (Exception e) {}
		return null;
	}
	
	public int toInt() {
		Integer integerVal = this.toInteger();
		return integerVal == null ? 0 : integerVal.intValue();
	}
	
	public Long toLong() {

		try{
			return new Long(this.string);
		}catch (Exception e) { }
		return null;
	}
	
	public Float toFloat() {
		
		try {
			return new Float(this.string);
		}catch (Exception e) { }
		return null;
	}
	
	public Double toDouble() {
		
		try {
			return new Double(this.string);
		}catch (Exception e) { }
		return null;
	}

	public Boolean toBoolean() {
		return possibleBooleanValues.get(this.string.toLowerCase());
	}

    public boolean isInArray(String[] arr) {

    	if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if(this.string.equals(arr[i]))
                	return true;
            }
        }
        return false;
    }

    public boolean isInArray(NGString[] arr) {

    	if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if(this.string.equals(arr[i].string))
                	return true;
            }
        }
        return false;
    }
    
    public NGString[] split(String regex) {
    	String []strArray = this.splitToStringArray(regex);
    	NGString[] ngStrings = new NGString[strArray.length];
    	for(int i = 0; i< strArray.length; i++)	{
    		ngStrings[i] = new NGString(strArray[i]);
    	}
    	return ngStrings;
	}
    
    public String[] splitToStringArray(String regex) {
    	return this.string.split(regex);
	}
}